<?php

use App\Kabupaten;
use App\Kecamatan;
use App\JenisPelayanan;


function partial($filename) {
	return view ( 'partial.' . $filename );
}
function pages($filename) {
	return view ( 'pages.' . $filename );
}
function script($filename){
    return '<script src="'+url('')+'/assets/js/pages/'+$filename+'.js"></script>';
}

function kabupaten(){
	return Kabupaten::all();
}

function kecamatan(){
    return Kecamatan::all();
}

function jenis_pelayanan(){
    return JenisPelayanan::all();
}



function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
} 

function calculateAge($dob){
  if(!empty($dob)){
    $birthdate = new DateTime($dob);
    $today   = new DateTime('today');
    $age = $birthdate->diff($today)->y;
    return $age;
}else{
    return 0;
}
}

function nameize($str,$a_char = array("'","-"," ")){    
    //$str contains the complete raw name string
    //$a_char is an array containing the characters we use as separators for capitalization. If you don't pass anything, there are three in there as default.
    $string = strtolower($str);
    foreach ($a_char as $temp){
        $pos = strpos($string,$temp);
        if ($pos){
            //we are in the loop because we found one of the special characters in the array, so lets split it up into chunks and capitalize each one.
            $mend = '';
            $a_split = explode($temp,$string);
            foreach ($a_split as $temp2){
                //capitalize each portion of the string which was separated at a special character
                $mend .= ucfirst($temp2).$temp;
            }
            $string = substr($mend,0,-1);
        }    
    }
    return ucfirst($string);
}


?>