<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $table = 'kecamatan';

    function kelurahan(){
    	return $this->hasMany('App\Kelurahan','id_kecamatan','id');
    }

    function kabupaten(){
    	return $this->hasOne('App\kabupaten','id','id_kabupaten');
    }
}
