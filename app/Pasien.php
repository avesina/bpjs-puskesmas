<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
    protected $table = 'pasien';

    function kabupaten(){
        return $this->hasOne('App\Kabupaten','id','id_kabupaten');
    }

    function kecamatan(){
    	return $this->hasOne('App\Kecamatan','id','id_kecamatan');
    }

    function kelurahan(){
    	return $this->hasOne('App\Kelurahan','id','id_kelurahan');
    }

    function periksa(){
    	return $this->hasMany('App\Pemeriksaan','id','id_pasien');
    }
}
