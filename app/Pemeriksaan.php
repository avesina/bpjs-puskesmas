<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pasien;

class Pemeriksaan extends Model
{
    protected $table = 'pemeriksaan';

    function pasien(){
    	return $this->belongsTo('App\Pasien','id_pasien');
    }

    function pelayanan(){
    	return $this->belongsTo('App\JenisPelayanan','id_jenis_pelayanan');
    }

    function diagnosis(){
    	return $this->hasOne('App\Diagnosis','id','id_diagnosis');
    }

    function dokter(){
    	return $this->hasOne('App\User','id','id_dokter');
    }
}

