<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pasien;
use App\Kelurahan;
use App\Kecamatan;
use App\Kabupaten;
use Validator;
use Excel;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

class PasienController extends Controller {
	private $rules_tambah_pasien = array(
		'noktp' => 'required',
		'nama_lengkap' => 'required',
		'tgl_lahir' => 'required',
		'alamat' => 'required'     
		);

	function index(){
		view()->share('data',Pasien::all());
		return view('layout.index')->with('page','pasien.list');
	}

	function create(){
		return view('layout.index')->with('page','pasien.add');
	}

	function store(Request $request){
		$v = Validator::make($request->all(), $this->rules_tambah_pasien);
		if ($v->fails())
		{
			$errors = $v->errors()->all();
			$json = ['status'=>false,'message'=>$errors];
		}else{
			$pasien = new Pasien();
			$pasien->no_rekam_medis = $request->no_rekam_medis;
			$pasien->no_rekam_medis_lama = $request->no_rekam_medis_lama;
			$pasien->noktp = $request->noktp;
			$pasien->nobpjs = $request->nobpjs;
			$pasien->kdfaskes = $request->kdfaskes;
			$pasien->namafaskes = $request->namafaskes;
			$pasien->noka = $request->noka;
			$pasien->nokk = $request->nokk;
			$pasien->nosin = $request->nosin;
			$pasien->nama_lengkap = nameize($request->nama_lengkap);
			$pasien->jenis_kelamin = $request->jenis_kelamin;
			$pasien->tgl_lahir = $request->tgl_lahir;
			$pasien->pisa = $request->pisa;
			$pasien->alamat = $request->alamat;
			$pasien->id_kabupaten = $request->id_kabupaten;
			$pasien->id_kecamatan = $request->id_kecamatan;
			$pasien->id_kelurahan = $request->id_kelurahan;
			$pasien->save();
			$json = ['status'=>true,'message'=>['Submit success']];
		}
		return $json;
	}

	function list_pasien(Request $request){
		$keyword = '%'.$request->input('query').'%';
		$sugest = [];

		// nama lengkap 
		$data = Pasien::Where('nama_lengkap','like',nameize($keyword))->get();
		
		foreach ($data as $key => $value) {
			$jenis_kunjungan = 'baru';
			if($value->periksa()->count()>0){
				$jenis_kunjungan = 'lama';
			}
			$sugest[] = ['value'=>$value->nama_lengkap,'data'=>$value,'jenis_kunjungan'=>$jenis_kunjungan];
		}

		// nomer bpjs
		$data = Pasien::Where('nobpjs','like',$keyword)->get();
		foreach ($data as $key => $value) {
			$jenis_kunjungan = 'baru';
			if($value->periksa()->count()>0){
				$jenis_kunjungan = 'lama';
			}
			$sugest[] = ['value'=>$value->nobpjs,'data'=>$value,'jenis_kunjungan'=>$jenis_kunjungan];
		}

		// no KTP
		$data = Pasien::Where('noktp','like',$keyword)->get();
		foreach ($data as $key => $value) {
			$jenis_kunjungan = 'baru';
			if($value->periksa()->count()>0){
				$jenis_kunjungan = 'lama';
			}
			$sugest[] = ['value'=>$value->noktp,'data'=>$value,'jenis_kunjungan'=>$jenis_kunjungan];
		}

		// // no Rekam Medis baru
		$data = Pasien::Where('no_rekam_medis','like',$keyword)->get();
		foreach ($data as $key => $value) {
			$jenis_kunjungan = 'baru';
			if($value->periksa()->count()>0){
				$jenis_kunjungan = 'lama';
			}
			$sugest[] = ['value'=>$value->no_rekam_medis,'data'=>$value,'jenis_kunjungan'=>$jenis_kunjungan];
		}

		// // no Rekam Medis lama
		$data = Pasien::Where('no_rekam_medis_lama','like',$keyword)->get();
		foreach ($data as $key => $value) {
			$jenis_kunjungan = 'baru';
			if($value->periksa()->count()>0){
				$jenis_kunjungan = 'lama';
			}
			$sugest[] = ['value'=>$value->no_rekam_medis_lama,'data'=>$value,'jenis_kunjungan'=>$jenis_kunjungan];
		}

		$json = ["suggestions"=>$sugest];
		return json_encode($json);
	}

	function list_data(Request $request){
		$draw = $request->draw;
		$skip = $request->start;
		$limit = $request->length;
		$search = $request->search;
		$countTotal = Pasien::all()->count();
		$searchTotal = $countTotal;
		$data = Pasien::select('*');
		if($search['value']){
			$keyword = $search['value'];
			$keyword_up = strtoupper($keyword);
			$keyword_low = mb_strtolower($keyword);
			$data = $data->where('nama_lengkap', 'like', '%'.$search['value'].'%')
			->orWhere('nama_lengkap','like','%'.$keyword_low.'%')
			->orWhere('nobpjs', 'like', '%'.$keyword_up.'%');
			$searchTotal = $data->count();
		}
		if($limit){
			$data->limit($limit);
		}
		if($skip){
			$data->skip($skip);
		}
		$data = $data->get()->map(function($item){
			$array = $item->toArray();
			$array['kabupaten'] = (($item->kabupaten)?$item->kabupaten->nama_kabupaten:'');
			$array['kecamatan'] = (($item->kecamatan)?$item->kecamatan->nama_kecamatan:'');
			$array['kelurahan'] = (($item->kelurahan)?$item->kelurahan->nama_kelurahan:'');
			$array['umur'] = calculateAge($item->tgl_lahir);
			return $array;
		});
		$arrayName = array('draw' => $draw,'recordsTotal'=>$countTotal,'recordsFiltered'=>$searchTotal,'data'=>$data);
		return json_encode($arrayName);
	}

	function edit($id){
		$pasien = Pasien::find($id);
		view()->share('pasien',$pasien);
		return view('layout.index')->with('page','pasien.add');
	}

	function update(Request $request,$id){
		$v = Validator::make($request->all(), $this->rules_tambah_pasien);
		if ($v->fails())
		{
			$errors = $v->errors()->all();
			$json = ['status'=>false,'message'=>$errors];
		}else{
			$pasien = Pasien::find($id);
			$pasien->no_rekam_medis = $request->no_rekam_medis;
			$pasien->no_rekam_medis_lama = $request->no_rekam_medis_lama;
			$pasien->noktp = $request->noktp;
			$pasien->nobpjs = $request->nobpjs;
			$pasien->kdfaskes = $request->kdfaskes;
			$pasien->namafaskes = $request->namafaskes;
			$pasien->noka = $request->noka;
			$pasien->nokk = $request->nokk;
			$pasien->nosin = $request->nosin;
			$pasien->nama_lengkap = nameize($request->nama_lengkap);
			$pasien->jenis_kelamin = $request->jenis_kelamin;
			$pasien->tgl_lahir = $request->tgl_lahir;
			$pasien->pisa = $request->pisa;
			$pasien->alamat = $request->alamat;
			$pasien->id_kabupaten = $request->id_kabupaten;
			$pasien->id_kecamatan = $request->id_kecamatan;
			$pasien->id_kelurahan = $request->id_kelurahan;
			$pasien->save();
			$json = ['status'=>true,'message'=>['Submit success']];
		}
		return $json;
	}
	
	function import(Request $request){
		// ini_set('memory_limit', '2048M'); set_time_limit('1200');
		$file = $request->file('excel');
		$reader = ReaderFactory::create(Type::XLSX);
		$reader->open($file);
		$jk = ["P"=>"female","L"=>"male"];

		foreach ($reader->getSheetIterator() as $sheet) {
			//dd($sheet);
			foreach ($sheet->getRowIterator() as $key => $row) {
            // $row is the first row of the sheet. Do something with it
            //break; // you won't read any other rows
			if($key != 1){
				$no_rekam_medis_lama = $row[2];
				if(is_string($row[3])){
					$nama = nameize($row[3]);
				}else{
					$nama = 'silakah cek lagi nama ini';
				}
				
				$jenis_kelamin = $jk[$row[7]];
				if(is_a($row[5], 'DateTime')){
					$tgl_lahir = $row[5]->format('Y-m-d');
				}else{
					$tgl_lahir = null;
				}
				
				$id_kelurahan = $this->cari_kelurahan($row[10]);
				$id_kecamatan = $this->cari_kecamatan($row[11]);
				$id_kabupaten = $this->cari_kabupaten($row[12]);
				$n = Pasien::where('nama_lengkap',$nama)->where('jenis_kelamin',$jenis_kelamin)->get()->count();
				if($n==0){
					$pasien = new Pasien();
					$pasien->no_rekam_medis_lama = $no_rekam_medis_lama;
					$pasien->nama_lengkap = $nama;
					$pasien->jenis_kelamin = $jenis_kelamin;
					$pasien->tgl_lahir = $tgl_lahir;
					$pasien->id_kelurahan = $id_kelurahan;
					$pasien->id_kecamatan = $id_kecamatan;
					$pasien->id_kabupaten = $id_kabupaten;
					$pasien->save();
				}
			}
        }
        break; // if you only want to read the first sheet
    	}

    	$reader->close();
	}

	function cari_kelurahan($nama){
		$data = Kelurahan::where('nama_kelurahan',strtolower($nama))->first();
		if($data){
			return $data->id;
		}else{
			return null;
		}
	}

	function cari_kecamatan($nama){
		$data = Kecamatan::where('nama_kecamatan',strtolower($nama))->first();
		if($data){
			return $data->id;
		}else{
			return null;
		}
	}

	function cari_kabupaten($nama){
		$data = Kabupaten::where('nama_kabupaten',strtolower($nama))->first();
		if($data){
			return $data->id;
		}else{
			return null;
		}
	}
}
?>