<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Kelurahan;
use Validator;

class KelurahanController extends Controller {
	private $rules_tambah_kelurahan = array(
		'nama_kelurahan' => 'required|unique:kelurahan',
		'id_kecamatan' => 'required'
		);

	public function select(Request $request){
		$id_kecamatan = $request->id_kecamatan;
		return Kelurahan::where('id_kecamatan',$id_kecamatan)->get()->toArray();
	}

	function index(){
		view()->share('data',Kelurahan::all());
		return view('layout.index')->with('page','kelurahan.list');
	}

	function create(){
		return view('layout.index')->with('page','kelurahan.add');
	}

	function store(Request $request){
		$v = Validator::make($request->all(), $this->rules_tambah_kelurahan);
		if ($v->fails())
		{
			$errors = $v->errors()->all();
			$json = ['status'=>false,'message'=>$errors];
		}else{
			$n_kel = explode(',',$request->nama_kelurahan);
			foreach ($n_kel as $key => $value) {
				$kec = new Kelurahan();
				$kec->nama_kelurahan = strtolower($value);
				$kec->id_kecamatan = $request->id_kecamatan;
				if($kec->save()){
					$json = ['status'=>true,'message'=>['Berhasil menyimpan data']];
				}else{
					$json = ['status'=>false,'message'=>['Ada Kesalahan']];
				}
			}
		}
		return $json;
	}

	function destroy($id){
		if(Kelurahan::find($id)->delete()){
			$json = ['status'=>true,'message'=>['Berhasil mendelete data']];
		}else{
			$json = ['status'=>false,'message'=>['Something wrond happen']];
		}
		return $json;
	}
}