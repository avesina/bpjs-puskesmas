<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Pemeriksaan;
use Validator;
use DB;

class PendaftaranController extends Controller {
	private $rules_tambah_pendaftaran = array(
		'id_pasien' => 'required',
		'tgl_pendaftaran' => 'required',
		'jenis_kunjungan' => 'required',
		'perawatan' => 'required',
		'jenis_kunjungan' => 'required',
		'jenis_pembayaran' => 'required',
		'jenis_pelayanan' => 'required'   
		);
	function create(Request $request){
		return view('layout.index')->with('page','pemeriksaan.pendaftaran');
	}

	function index(Request $request){
		$data = Pemeriksaan::where('status','daftar')->get();
			view()->share('data',$data);
		return view('layout.index')->with('page','pendaftaran.list');
	}

	function show($id){
		if($id == 'sekarang'){
			$data = Pemeriksaan::where('status','daftar')->where(DB::raw('DATE(created_at)'),date('Y-m-d'))->get();
			view()->share('data',$data);
			return view('layout.index')->with('page','pendaftaran.list');
		}
	}

	function store(Request $request){
		$v = Validator::make($request->all(), $this->rules_tambah_pendaftaran);
		if ($v->fails())
		{
			$errors = $v->errors()->all();
			$json = ['status'=>false,'message'=>$errors];
		}else{
			$daftar = new Pemeriksaan();
			$daftar->id_pasien = $request->id_pasien;
			$daftar->tgl_pendaftaran = $request->tgl_pendaftaran;
			$daftar->jenis_kunjungan = $request->jenis_kunjungan;
			$daftar->perawatan = $request->perawatan;
			$daftar->jenis_pembayaran = $request->jenis_pembayaran;
			$daftar->id_jenis_pelayanan = $request->jenis_pelayanan;
			$daftar->jenis_kunjungan = $request->jenis_kunjungan;
			$daftar->tinggi_badan = $request->tinggi_badan;
			$daftar->berat_badan = $request->berat_badan;
			$daftar->sistole = $request->sistole;
			$daftar->diastole = $request->diastole;
			$daftar->respiratory_rate = $request->respiratory_rate;
			$daftar->heart_rate = $request->heart_rate;
			$daftar->keluhan = $request->keluhan;
			$daftar->status = 'daftar';
			if($daftar->save()){
				$json = ['status'=>true,'message'=>['Submit success']];
			}else{
				$json = ['status'=>false,'message'=>['Submit unsuccess']];
			}
		}
		return $json;
	}

	function list_data(Request $request){
		$data = Pemeriksaan::where('status','daftar')->where(DB::raw('DATE(created_at)'),date('Y-m-d'))->get();
		view()->share('data',$data);
		return view('layout.index')->with('page','pemeriksaan.listdaftar');
	}	

}