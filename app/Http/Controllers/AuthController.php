<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Session;

class AuthController extends Controller {
	function login(){
		return view('layout.login');
	}
	function post_login(Request $request){
		// dd($request->username);
		$user = User::where('username','=',$request->username)->first();
		$string = str_random(40);
		// dd($user);
		if($user){
			if($request->password == $user->password){
				Session::put('nuravesinamustari',$string);
				Session::put($string,$user);
				return redirect('/');
			}else{
				Session::flush();
				return redirect('/login');
			}
		}else{
			Session::flush();
			return redirect('/login');
		}
		
	}
}