<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Pemeriksaan;
use Illuminate\Http\Request;
use Validator;
use Session;

class PelayananController extends Controller {
	private $rules_update_pemeriksaan = array(
		'jenis_kunjungan' => 'required',
		'perawatan' => 'required',
		'jenis_kunjungan' => 'required',
		'jenis_kasus' => 'required',
		'id_diagnosis' => 'required',
		'keluhan' => 'required',
		'terapi' => 'required',

		);
	function show(Request $request,$id){
		view()->share('pemeriksaan',Pemeriksaan::find($id));
		return view('layout.index')->with('page','pemeriksaan.edit');
	}

	function index(){
		view()->share('data',Pemeriksaan::where('status','rawat')->get());
		return view('layout.index')->with('page','pemeriksaan.list');
	}	

	function update(Request $request,$id){
		$v = Validator::make($request->all(), $this->rules_update_pemeriksaan);
		if ($v->fails())
		{
			$errors = $v->errors()->all();
			$json = ['status'=>false,'message'=>$errors];
		}else{
			$key = Session::get('nuravesinamustari');
			$user = Session::get($key);
			$daftar = Pemeriksaan::find($id);
			$daftar->id_dokter = $user->id;
			$daftar->tgl_pendaftaran = $request->tgl_pendaftaran;
			$daftar->jenis_kunjungan = $request->jenis_kunjungan;
			$daftar->perawatan = $request->perawatan;
			$daftar->jenis_pembayaran = $request->jenis_pembayaran;
			$daftar->id_jenis_pelayanan = $request->jenis_pelayanan;
			$daftar->jenis_kunjungan = $request->jenis_kunjungan;
			$daftar->tinggi_badan = $request->tinggi_badan;
			$daftar->berat_badan = $request->berat_badan;
			$daftar->sistole = $request->sistole;
			$daftar->diastole = $request->diastole;
			$daftar->respiratory_rate = $request->respiratory_rate;
			$daftar->heart_rate = $request->heart_rate;
			$daftar->keluhan = $request->keluhan;
			$daftar->id_diagnosis = $request->id_diagnosis;
			$daftar->jenis_kasus = $request->jenis_kasus;
			$daftar->terapi = $request->terapi;
			$daftar->keterangan = $request->keterangan;
			$daftar->status = 'rawat';
			if($daftar->save()){
				$json = ['status'=>true,'message'=>['Submit success']];
			}else{
				$json = ['status'=>false,'message'=>['Submit unsuccess']];
			}
		}
		return $json;
	}

	function list_data(Request $request){
		$draw = $request->draw;
		$skip = $request->start;
		$limit = $request->length;
		$search = $request->search;
		$countTotal = Pemeriksaan::where('status','rawat')->count();
		$searchTotal = $countTotal;
		$data = Pemeriksaan::select('*')->where('status','rawat');
		if($search['value']){
			$keyword = $search['value'];
			$keyword_up = strtoupper($keyword);
			$keyword_low = mb_strtolower($keyword);
			$data = $data->where('nama_lengkap', 'like', '%'.$search['value'].'%')
			->orWhere('nama_lengkap','like','%'.$keyword_low.'%')
			->orWhere('nobpjs', 'like', '%'.$keyword_up.'%');
			$searchTotal = $data->count();
		}
		if($limit){
			$data->limit($limit);
		}
		if($skip){
			$data->skip($skip);
		}
		$data = $data->get()->map(function($item){
			//$array = $item->toArray();
			//$array['kabupaten'] = (($item->kabupaten)?$item->kabupaten->nama_kabupaten:'');
			//$array['kecamatan'] = (($item->kecamatan)?$item->kecamatan->nama_kecamatan:'');
			//$array['kelurahan'] = (($item->kelurahan)?$item->kelurahan->nama_kelurahan:'');
			$array['id'] = $item->id;
			$array['tanggal'] = date('Y-m-d H:i:s',strtotime($item->created_at));
			$array['nama_lengkap'] = $item->pasien->nama_lengkap;
			$array['umur'] = calculateAge($item->pasien->tgl_lahir);
			$array['jenis_pembayaran'] = $item->jenis_pembayaran;
			$array['jenis_pelayanan'] = $item->jenis_pelayanan;
			$array['jenis_kunjungan'] = $item->jenis_kunjungan;
			$array['keluhan'] = $item->keluhan;
			$array['diagnosis'] = $item->diagnosis->nama_diagnosis;
			return $array;
		});
		$arrayName = array('draw' => $draw,'recordsTotal'=>$countTotal,'recordsFiltered'=>$searchTotal,'data'=>$data);
		return json_encode($arrayName);
	}
}