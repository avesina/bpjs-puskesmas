<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Kabupaten;
use Validator;

class KabupatenController extends Controller {
	private $rules_tambah_kabupaten = array('nama_kabupaten' =>'required|unique:kabupaten');

	function index(){
		view()->share('data',Kabupaten::all());
		return view('layout.index')->with('page','kabupaten.list');
	}

	function create(){
		return view('layout.index')->with('page','kabupaten.add');
	}

	function store(Request $request){
		$v = Validator::make($request->all(), $this->rules_tambah_kabupaten);
		if ($v->fails())
		{
			$errors = $v->errors()->all();
			$json = ['status'=>false,'message'=>$errors];
		}else{
			$n_kab = explode(',',$request->nama_kabupaten);
			foreach ($n_kab as $key => $value) {
				$kab = new Kabupaten();
				$kab->nama_kabupaten = strtolower($value);

				if($kab->save()){
					$json = ['status'=>true,'message'=>['Berhasil menyimpan data']];
				}else{
					$json = ['status'=>false,'message'=>['Something wrond happen']];
				}
			}
			
		}
		return $json;
	}

	function destroy($id){
		if(Kabupaten::find($id)->delete()){
			$json = ['status'=>true,'message'=>['Berhasil mendelete data']];
		}else{
			$json = ['status'=>false,'message'=>['Something wrond happen']];
		}
		return $json;
	}
}