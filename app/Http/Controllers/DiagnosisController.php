<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Pemeriksaan;
use Illuminate\Http\Request;
use App\Diagnosis;
use Validator;
use Excel;

class DiagnosisController extends Controller {
	private $rules_tambah_diagnosis = array(
		'nama_diagnosis' => 'required|unique:diagnosis',
		'keterangan' => 'required'
		);

	function index(){
		view()->share('data',Diagnosis::all());
		return view('layout.index')->with('page','diagnosis.list');
	}

	function edit($id){
		view()->share('diagnosis',Diagnosis::find($id));
		return view('layout.index')->with('page','diagnosis.add');
	}

	function create(){
		return view('layout.index')->with('page','diagnosis.add');
	}

	function store(Request $request){
		$v = Validator::make($request->all(), $this->rules_tambah_diagnosis);
		if ($v->fails())
		{
			$errors = $v->errors()->all();
			$json = ['status'=>false,'message'=>$errors];
		}else{
			$diag = new Diagnosis();
			$diag->nama_diagnosis = $request->nama_diagnosis;
			$diag->keterangan = $request->keterangan;
			if($diag->save()){
				$json = ['status'=>true,'message'=>['Submit success']];
			}else{
				$json = ['status'=>false,'message'=>['Submit unsuccess']];
			}	
		}
		return $json;
	}

	function destroy($id){
		if(Diagnosis::find($id)->delete()){
			$json = ['status'=>true,'message'=>['Berhasil mendelete data']];
		}else{
			$json = ['status'=>false,'message'=>['Something wrond happen']];
		}
		return $json;
	}

	function list_data(Request $request){
		$draw = $request->draw;
		$skip = $request->start;
		$limit = $request->length;
		$search = $request->search;
		$countTotal = Diagnosis::all()->count();
		$searchTotal = $countTotal;
		$data = Diagnosis::select('id','kode','nama_diagnosis');
		if($search['value']){
			$keyword = $search['value'];
			$keyword_up = strtoupper($keyword);
			$keyword_low = mb_strtolower($keyword);
			$data = $data->where('nama_diagnosis', 'like', '%'.$search['value'].'%')
					->orWhere('nama_diagnosis','like','%'.$keyword_low.'%')
					->orWhere('kode', 'like', '%'.$keyword_up.'%');
			$searchTotal = $data->count();
		}
		if($limit){
			$data->limit($limit);
		}
		if($skip){
			$data->skip($skip);
		}
		$data = $data->get()->toArray();
		$arrayName = array('draw' => $draw,'recordsTotal'=>$countTotal,'recordsFiltered'=>$searchTotal,'data'=>$data);
		return json_encode($arrayName);
	}

	function import(Request $request){
		$file = $request->file('excel');
		Excel::load($file, function($reader) {
			foreach($reader as $sheet)
			{
   				// dd($sheet);
			}
			// dd('..');
			$reader->each(function($sheet) {
				$data = $sheet->toArray();
				$kode = $data['kode_icd_10'];
				$diagnosis = $data['diagnosa'];
				$keterangan = $data['diskripsi'];
				$n = Diagnosis::where('kode',$kode)->get()->count();
				if($n == 0){
					$diagnosa = new Diagnosis();
					$diagnosa->kode = $kode;
					$diagnosa->nama_diagnosis  = $diagnosis;
					$diagnosa->keterangan = $keterangan;
					$diagnosa->save();
					echo($diagnosis.' - saved .... <br>');
				}

			});
			$reader->close();
		});
	}
}