<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Pemeriksaan;
use Excel;
use DB;

class DashboardController extends Controller {
	function index(){
		return view('layout/index')->with('page','dashboard.index');
	}

	function stat(){

		$daily = Pemeriksaan::where('status','rawat')->where(DB::raw('DATE(created_at)'),"".date('Y-m-d')."")->get()->count();
		$monthly = Pemeriksaan::where('status','rawat')->where(DB::raw('DATE(created_at)'),'>',"".date('Y-m-01')."")->where(DB::raw('DATE(created_at)'),'<',"".date('Y-m-31')."")->get()->count();
		$yearly = Pemeriksaan::where('status','rawat')->where(DB::raw('DATE(created_at)'),'>',"".date('Y-01-01')."")->where(DB::raw('DATE(created_at)'),'<',"".date('Y-12-31')."")->get()->count();
		$total = Pemeriksaan::where('status','rawat')->get()->count();
		$list = Pemeriksaan::select('diagnosis.nama_diagnosis',DB::raw('count(id_diagnosis) as jumlah'))->join('diagnosis','diagnosis.id','=','pemeriksaan.id_diagnosis')->groupBy('id_diagnosis')->orderBy('jumlah','DESC')->get();

		$data = Pemeriksaan::select('tgl_pendaftaran',DB::raw('COUNT(pemeriksaan.id) as n'))->where('status','rawat')->groupBy(DB::raw('DATE(tgl_pendaftaran)'))->get();
		$json = [];
		foreach ($data as $key => $value) {
			$json [] = [strtotime($value->tgl_pendaftaran)*1000,$value->n];
		}

		//return $json;

		return [
			'total'=>$total,
			'year'=>$yearly,
			'monthly'=>$monthly,
			'today'=>$daily,
			'listpain'=>$list->toArray(),
			'data'=>$json
		];
	}

	function grafik(){
		$data = Pemeriksaan::select('tgl_pendaftaran',DB::raw('COUNT(pemeriksaan.id) as n'))->where('status','rawat')->groupBy(DB::raw('DATE(tgl_pendaftaran)'))->get();
		$json = [];
		foreach ($data as $key => $value) {
			$json [] = [strtotime($value->tgl_pendaftaran)*1000,$value->n];
		}

		return $json;
	}	
}