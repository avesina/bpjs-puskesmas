<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Kecamatan;
use Validator;

class KecamatanController extends Controller {
	private $rules_tambah_kecamatan = array(
		'nama_kecamatan' => 'required|unique:kecamatan',
		'id_kabupaten' => 'required'
		);

	public function select(Request $request){
		$id_kabupaten = $request->id_kabupaten;
		return Kecamatan::where('id_kabupaten',$id_kabupaten)->get()->toArray();
	}

	function index(){
		view()->share('data',Kecamatan::all());
		return view('layout.index')->with('page','kecamatan.list');
	}

	function create(){
		return view('layout.index')->with('page','kecamatan.add');
	}

	function store(Request $request){
		$v = Validator::make($request->all(), $this->rules_tambah_kecamatan);
		if ($v->fails())
		{
			$errors = $v->errors()->all();
			$json = ['status'=>false,'message'=>$errors];
		}else{
			$n_kec = explode(',',$request->nama_kecamatan);
			foreach ($n_kec as $key => $value) {
				$kec = new Kecamatan();
				$kec->nama_kecamatan = strtolower($value);
				$kec->id_kabupaten = $request->id_kabupaten;
				if($kec->save()){
					$json = ['status'=>true,'message'=>['Berhasil menyimpan data']];
				}else{
					$json = ['status'=>false,'message'=>['Ada Kesalahan']];
				}
			}
		}
		return $json;
	}

	function destroy($id){
		if(Kecamatan::find($id)->delete()){
			$json = ['status'=>true,'message'=>['Berhasil mendelete data']];
		}else{
			$json = ['status'=>false,'message'=>['Something wrond happen']];
		}
		return $json;
	}
}