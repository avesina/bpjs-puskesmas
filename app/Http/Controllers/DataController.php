<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Pasien;
use App\Pemeriksaan;
use DB;

class DataController extends Controller {
	function range_umur($start,$end){
		view()->share('start',$start);
		view()->share('end',$end);
		$date_start = date('Y-m-d',$start);
		$date_end = date('Y-m-d',$end);
		$data = Pasien::select(
			'pasien.id_kelurahan','kelurahan.nama_kelurahan',
			DB::raw('SUM(CASE WHEN age_c = 0 AND julianday(date(pemeriksaan.tgl_pendaftaran))-julianday(date(tgl_lahir)) <= 7 THEN 1 ELSE 0 END) AS [0-7hari]'),
			DB::raw('SUM(CASE WHEN age_c = 0 AND julianday(date(pemeriksaan.tgl_pendaftaran))-julianday(date(tgl_lahir)) BETWEEN 8 AND 30 THEN 1 ELSE 0 END) AS [8-30hari]'),
			DB::raw('SUM(CASE WHEN age_c = 0 AND julianday(date(pemeriksaan.tgl_pendaftaran))-julianday(date(tgl_lahir)) BETWEEN 31 AND 340 THEN 1 ELSE 0 END) AS [1-11bulan]'),
			DB::raw('SUM(CASE WHEN age_c BETWEEN 1 AND 4 THEN 1 ELSE 0 END) AS [1-4]'),
			DB::raw('SUM(CASE WHEN age_c BETWEEN 5 AND 14 THEN 1 ELSE 0 END) AS [5-14]'),
			DB::raw('SUM(CASE WHEN age_c BETWEEN 15 AND 44 THEN 1 ELSE 0 END) AS [15-44]'),
			DB::raw('SUM(CASE WHEN age_c BETWEEN 45 AND 64 THEN 1 ELSE 0 END) AS [45-64]'),
			DB::raw('SUM(CASE WHEN age_c BETWEEN 45 AND 64 THEN 1 ELSE 0 END) AS [45-64]'),
			DB::raw('SUM(CASE WHEN age_c > 64 THEN 1 ELSE 0 END) AS [64more]')
		)
			->whereRaw('DATE(pemeriksaan.tgl_pendaftaran) BETWEEN "'.$date_start.'" AND "'.$date_end.'"')
			->join(DB::raw('(select pemeriksaan.*, date(pemeriksaan.tgl_pendaftaran)-date(pasien.tgl_lahir) as age_c from pasien join pemeriksaan on pasien.id = pemeriksaan.id_pasien) pemeriksaan'),
			'pemeriksaan.id_pasien','=','pasien.id')
			->join('kelurahan','kelurahan.id','=','pasien.id_kelurahan')->groupBy('pasien.id_kelurahan')->get();
		view()->share('data',$data);
		return view('layout.index')->with('page','data.range-umur');
	}

	function jenis_pembayaran($start,$end){
		view()->share('start',$start);
		view()->share('end',$end);
		$date_start = date('Y-m-d',$start);
		$date_end = date('Y-m-d',$end);

		$data = Pasien::select(
			'pasien.id_kelurahan','kelurahan.nama_kelurahan',
			DB::raw('SUM(CASE WHEN pemeriksaan.jenis_pembayaran == "umum" THEN 1 ELSE 0 END) AS umum'),
			DB::raw('SUM(CASE WHEN pemeriksaan.jenis_pembayaran == "bpjs" THEN 1 ELSE 0 END) AS bpjs'),
			DB::raw('SUM(CASE WHEN pemeriksaan.jenis_pembayaran == "lainnya" THEN 1 ELSE 0 END) AS lainnya')
		)
			->whereRaw('DATE(pemeriksaan.tgl_pendaftaran) BETWEEN "'.$date_start.'" AND "'.$date_end.'"')
			->join(DB::raw('(select pemeriksaan.*, date(pemeriksaan. tgl_pendaftaran)-date(pasien.tgl_lahir) as age_c from pasien join pemeriksaan on pasien.id = pemeriksaan.id_pasien) pemeriksaan'),
			'pemeriksaan.id_pasien','=','pasien.id')
			->join('kelurahan','kelurahan.id','=','pasien.id_kelurahan')->groupBy('pasien.id_kelurahan')->get();
		view()->share('data',$data);

		return view('layout.index')->with('page','data.jenis-pembayaran');
	}

	function jenis_kunjungan($start,$end){
		view()->share('start',$start);
		view()->share('end',$end);
		$date_start = date('Y-m-d',$start);
		$date_end = date('Y-m-d',$end);

		$data = Pasien::select(
			'pasien.id_kelurahan','kelurahan.nama_kelurahan',
			DB::raw('SUM(CASE WHEN pemeriksaan.jenis_kunjungan == "baru" THEN 1 ELSE 0 END) AS baru'),
			DB::raw('SUM(CASE WHEN pemeriksaan.jenis_kunjungan == "lama" THEN 1 ELSE 0 END) AS lama')
		)
			->whereRaw('DATE(pemeriksaan.tgl_pendaftaran) BETWEEN "'.$date_start.'" AND "'.$date_end.'"')
			->join(DB::raw('(select pemeriksaan.*, date(pemeriksaan. tgl_pendaftaran)-date(pasien.tgl_lahir) as age_c from pasien join pemeriksaan on pasien.id = pemeriksaan.id_pasien) pemeriksaan'),
			'pemeriksaan.id_pasien','=','pasien.id')
			->join('kelurahan','kelurahan.id','=','pasien.id_kelurahan')->groupBy('pasien.id_kelurahan')->get();
		view()->share('data',$data);
		
		return view('layout.index')->with('page','data.jenis-kunjungan');
	}

	function jenis_kasus($start,$end){
		view()->share('start',$start);
		view()->share('end',$end);
		$date_start = date('Y-m-d',$start);
		$date_end = date('Y-m-d',$end);

		$data = Pasien::select(
			'pasien.id_kelurahan','kelurahan.nama_kelurahan',
			DB::raw('SUM(CASE WHEN pemeriksaan.jenis_kasus == "baru" THEN 1 ELSE 0 END) AS baru'),
			DB::raw('SUM(CASE WHEN pemeriksaan.jenis_kasus == "lama" THEN 1 ELSE 0 END) AS lama')
		)
			->whereRaw('DATE(pemeriksaan.tgl_pendaftaran) BETWEEN "'.$date_start.'" AND "'.$date_end.'"')
			->join(DB::raw('(select pemeriksaan.*, date(pemeriksaan. tgl_pendaftaran)-date(pasien.tgl_lahir) as age_c from pasien join pemeriksaan on pasien.id = pemeriksaan.id_pasien) pemeriksaan'),
			'pemeriksaan.id_pasien','=','pasien.id')
			->join('kelurahan','kelurahan.id','=','pasien.id_kelurahan')->groupBy('pasien.id_kelurahan')->get();
		view()->share('data',$data);

		return view('layout.index')->with('page','data.jenis-kasus');
	}

	function jenis_penyakit($start,$end){
		view()->share('start',$start);
		view()->share('end',$end);
		$date_start = date('Y-m-d',$start);
		$date_end = date('Y-m-d',$end);
		$data = Pasien::select(
			'pemeriksaan.id_diagnosis','diagnosis.nama_diagnosis',
			DB::raw('SUM(CASE WHEN age_c = 0 AND julianday(date(pemeriksaan.tgl_pendaftaran))-julianday(date(tgl_lahir)) <= 7 THEN 1 ELSE 0 END) AS [0-7hari]'),
			DB::raw('SUM(CASE WHEN age_c = 0 AND julianday(date(pemeriksaan.tgl_pendaftaran))-julianday(date(tgl_lahir)) BETWEEN 8 AND 30 THEN 1 ELSE 0 END) AS [8-30hari]'),
			DB::raw('SUM(CASE WHEN age_c = 0 AND julianday(date(pemeriksaan.tgl_pendaftaran))-julianday(date(tgl_lahir)) BETWEEN 31 AND 340 THEN 1 ELSE 0 END) AS [1-11bulan]'),
			DB::raw('SUM(CASE WHEN age_c BETWEEN 1 AND 4 THEN 1 ELSE 0 END) AS [1-4]'),
			DB::raw('SUM(CASE WHEN age_c BETWEEN 5 AND 14 THEN 1 ELSE 0 END) AS [5-14]'),
			DB::raw('SUM(CASE WHEN age_c BETWEEN 15 AND 44 THEN 1 ELSE 0 END) AS [15-44]'),
			DB::raw('SUM(CASE WHEN age_c BETWEEN 45 AND 64 THEN 1 ELSE 0 END) AS [45-64]'),
			DB::raw('SUM(CASE WHEN age_c BETWEEN 45 AND 64 THEN 1 ELSE 0 END) AS [45-64]'),
			DB::raw('SUM(CASE WHEN age_c > 64 THEN 1 ELSE 0 END) AS [64more]')
		)
			->whereRaw('DATE(pemeriksaan.tgl_pendaftaran) BETWEEN "'.$date_start.'" AND "'.$date_end.'"')
			->where('status','rawat')
			->join(DB::raw('(select pemeriksaan.*, date(pemeriksaan.tgl_pendaftaran)-date(pasien.tgl_lahir) as age_c from pasien join pemeriksaan on pasien.id = pemeriksaan.id_pasien) pemeriksaan'),
			'pemeriksaan.id_pasien','=','pasien.id')
			->join('diagnosis','diagnosis.id','=','pemeriksaan.id_diagnosis')->groupBy('pemeriksaan.id_diagnosis')->get();

		view()->share('data',$data);

		return view('layout.index')->with('page','data.jenis-penyakit');

	}

	function chart_penyakit($start,$end){
		view()->share('start',$start);
		view()->share('end',$end);
		$date_start = date('Y-m-d',$start);
		$date_end = date('Y-m-d',$end);

		return view('layout.index')->with('page','data.chart-penyakit');
	}

	function chart_penyakit_json(Request $request,$start,$end){
		$tab = $request->tab;
		$date_start = date('Y-m-d',$start);
		$date_end = date('Y-m-d',$end);

		$data = Pasien::select(
			'pemeriksaan.id_diagnosis','diagnosis.nama_diagnosis',
			DB::raw('SUM(CASE WHEN age_c = 0 AND julianday(date(pemeriksaan.tgl_pendaftaran))-julianday(date(tgl_lahir)) <= 7 THEN 1 ELSE 0 END) AS [0-7hari]'),
			DB::raw('SUM(CASE WHEN age_c = 0 AND julianday(date(pemeriksaan.tgl_pendaftaran))-julianday(date(tgl_lahir)) BETWEEN 8 AND 30 THEN 1 ELSE 0 END) AS [8-30hari]'),
			DB::raw('SUM(CASE WHEN age_c = 0 AND julianday(date(pemeriksaan.tgl_pendaftaran))-julianday(date(tgl_lahir)) BETWEEN 31 AND 340 THEN 1 ELSE 0 END) AS [1-11bulan]'),
			DB::raw('SUM(CASE WHEN age_c BETWEEN 1 AND 4 THEN 1 ELSE 0 END) AS [1-4]'),
			DB::raw('SUM(CASE WHEN age_c BETWEEN 5 AND 14 THEN 1 ELSE 0 END) AS [5-14]'),
			DB::raw('SUM(CASE WHEN age_c BETWEEN 15 AND 44 THEN 1 ELSE 0 END) AS [15-44]'),
			DB::raw('SUM(CASE WHEN age_c BETWEEN 45 AND 64 THEN 1 ELSE 0 END) AS [45-64]'),
			DB::raw('SUM(CASE WHEN age_c BETWEEN 45 AND 64 THEN 1 ELSE 0 END) AS [45-64]'),
			DB::raw('SUM(CASE WHEN age_c > 64 THEN 1 ELSE 0 END) AS [64more]')
		)
			->whereRaw('DATE(pemeriksaan.tgl_pendaftaran) BETWEEN "'.$date_start.'" AND "'.$date_end.'"')
			->where('status','rawat')
			->join(DB::raw('(select pemeriksaan.*, date(pemeriksaan.tgl_pendaftaran)-date(pasien.tgl_lahir) as age_c from pasien join pemeriksaan on pasien.id = pemeriksaan.id_pasien) pemeriksaan'),
			'pemeriksaan.id_pasien','=','pasien.id')
			->join('diagnosis','diagnosis.id','=','pemeriksaan.id_diagnosis')->groupBy('pemeriksaan.id_diagnosis')->get();
		
		$dt = [];
		$ticks = [];
		if($tab == 1){
			foreach ($data as $key => $val) {
				$dt[] = [$key+1,$val->{'0-7hari'}];
				$ticks[] = [$key+1,$val->diagnosis];
			}
		}else if($tab == 2){
			foreach ($data as $key => $val) {
				$dt[] = [$key+1,$val->{'8-30hari'}];
				$ticks[] = [$key+1,$val->diagnosis];
			}
		}else if($tab == 3){
			foreach ($data as $key => $val) {
				$dt[] = [$key+1,$val->{'1-11bulan'}];
				$ticks[] = [$key+1,$val->diagnosis];
			}
		}else if($tab == 4){
			foreach ($data as $key => $val) {
				$dt[] = [$key+1,$val->{'1-4'}];
				$ticks[] = [$key+1,$val->diagnosis];
			}
		}else if ($tab == 5){
			foreach ($data as $key => $val) {
				$dt[] = [$key+1,$val->{'5-14'}];
				$ticks[] = [$key+1,$val->diagnosis];
			}
		}else if($tab == 6){
			foreach ($data as $key => $val) {
				$dt[] = [$key+1,$val->{'15-44'}];
				$ticks[] = [$key+1,$val->diagnosis];
			}
		}else if($tab == 7){
			foreach ($data as $key => $val) {
				$dt[] = [$key+1,$val->{'45-64'}];
				$ticks[] = [$key+1,$val->diagnosis];
			}
		}else if($tab == 8){
			foreach ($data as $key => $val) {
				$dt[] = [$key+1,$val->{'64more'}];
				$ticks[] = [$key+1,$val->diagnosis];
			}
		}else if($tab == 9){
			$data = Pasien::select('pemeriksaan.id_diagnosis','diagnosis.nama_diagnosis',DB::raw('COUNT(pemeriksaan.id) as n'))
			->whereRaw('DATE(pemeriksaan.tgl_pendaftaran) BETWEEN "'.$date_start.'" AND "'.$date_end.'"')
			->where('status','rawat')
			->join(DB::raw('(select pemeriksaan.*, date(pemeriksaan.tgl_pendaftaran)-date(pasien.tgl_lahir) as age_c from pasien join pemeriksaan on pasien.id = pemeriksaan.id_pasien) pemeriksaan'),
			'pemeriksaan.id_pasien','=','pasien.id')
			->join('diagnosis','diagnosis.id','=','pemeriksaan.id_diagnosis')->groupBy('pemeriksaan.id_diagnosis')->get();
			foreach ($data as $key => $val) {
				$dt[] = [$key+1,$val->n];
				$ticks[] = [$key+1,$val->diagnosis];
			}
		}


			
		return ['data'=>$dt,'ticks'=>$ticks];
	}

}