<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('login','AuthController@login');
Route::post('login','AuthController@post_login');
Route::get('logout',function(){
	Session::flush();
	return redirect(url(''));
});

Route::group(array('middleware'=>'auth'), function() 
{
	Route::get('/','DashboardController@index');
	Route::any('/dashboard','DashboardController@stat');
	Route::any('/dashboard/grafik','DashboardController@grafik');
	Route::get('import','CommonController@import');
	Route::resource('kabupaten','KabupatenController');
	Route::resource('kecamatan','KecamatanController');
	Route::resource('kelurahan','KelurahanController');
	Route::any('kecamatan/select','KecamatanController@select');
	Route::any('kelurahan/select','KelurahanController@select');
	Route::get('pasien/profile','PasienController@profile');
	Route::post('import/pasien','PasienController@import');
	Route::post('import/diagnosis','DiagnosisController@import');
	Route::any('pasien/list','PasienController@list_data');
	Route::resource('pasien','PasienController');
	Route::get('autocomplete/pasien','PasienController@list_pasien');
	Route::get('pendaftaran/list','PendaftaranController@list_data');
	Route::resource('pendaftaran','PendaftaranController');
	Route::any('pemeriksaan/list','PelayananController@list_data');
	Route::resource('pemeriksaan','PelayananController');
	Route::any('diagnosis/list','DiagnosisController@list_data');
	Route::resource('diagnosis','DiagnosisController');

	// olah data
	Route::get('data/rangeumur/{start}/sampai/{end}','DataController@range_umur');
	Route::get('data/jenispembayaran/{start}/sampai/{end}','DataController@jenis_pembayaran');
	Route::get('data/jeniskunjungan/{start}/sampai/{end}','DataController@jenis_kunjungan');
	Route::get('data/jeniskasus/{start}/sampai/{end}','DataController@jenis_kasus');
	Route::get('data/penyakit/{start}/sampai/{end}','DataController@jenis_penyakit');
	Route::get('data/penyakit/{start}/sampai/{end}','DataController@jenis_penyakit');
	Route::get('data/penyakit/chart/{start}/sampai/{end}','DataController@chart_penyakit');
	Route::get('data/penyakit/chart/{start}/sampai/{end}/json','DataController@chart_penyakit_json');

});

/* menu 
	- Dashboard
	- Pasien
	- Pelayanan
	- All Data
	DB
	- Pasien
		> id
		> kdfaskes
		> namafaskes
		> noka
		> nokk
		> nosin
		> nama_lengkap
		> tgl_lahir
		> pisa
		> alamat
		> kecamatan
		> kelurahan
	- Kecamatan
		> id
		> nama_kecamatan
	- Kelurahan
		> id
		> id_kecamatan
		> nama_kelurahan
	- Pemeriksaan
		> id
		> tgl_pendaftaran
		> jenis_kunjungan
		> perawatan
		> poli_tujuan
		> keluhan
		> tinggi_badan
		> berat_badan
		> sistole
		> diastole
		> respiratory_rate
		> heart_rate
		> keluhan
		> terapi
		> diagnosis
*/
