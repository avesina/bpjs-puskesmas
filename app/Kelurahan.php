<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    protected $table = 'kelurahan';

    function kecamatan(){
    	return $this->hasOne('App\Kecamatan','id','id_kecamatan');
    }
}
