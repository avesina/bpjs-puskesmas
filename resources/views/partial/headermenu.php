            <div class="navbar-inner container">
              <div class="sidebar-pusher">
                <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                  <i class="fa fa-bars"></i>
                </a>
              </div>
              <div class="logo-box">
                <a href="index.html" class="logo-text"><span>Sistem Informasi Rawat Jalan</span></a>
              </div><!-- Logo Box -->
              <div class="search-button">
                <a href="javascript:void(0);" class="waves-effect waves-button waves-classic show-search"><i class="fa fa-search"></i></a>
              </div>
              <div class="topmenu-outer">
                <div class="top-menu">
                  <ul class="nav navbar-nav navbar-right">
                    <li>  
                      <a href="javascript:void(0);" class="waves-effect waves-button waves-classic show-search"><i class="fa fa-search"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
                        <span class="user-name">David<i class="fa fa-angle-down"></i></span>
                        <img class="img-circle avatar" src="<?=url('')?>/assets/images/avatar1.png" width="40" height="40" alt="">
                      </a>
                      <ul class="dropdown-menu dropdown-list" role="menu">
                        <li role="presentation"><a href="profile.html"><i class="fa fa-user"></i>Profile</a></li>
                        <li role="presentation"><a href="calendar.html"><i class="fa fa-calendar"></i>Calendar</a></li>
                        <li role="presentation"><a href="inbox.html"><i class="fa fa-envelope"></i>Inbox<span class="badge badge-success pull-right">4</span></a></li>
                        <li role="presentation" class="divider"></li>
                        <li role="presentation"><a href="lock-screen.html"><i class="fa fa-lock"></i>Lock screen</a></li>
                        <li role="presentation"><a href="login.html"><i class="fa fa-sign-out m-r-xs"></i>Log out</a></li>
                      </ul>
                    </li>
                  </ul><!-- Nav -->
                </div><!-- Top Menu -->
              </div>
            </div>