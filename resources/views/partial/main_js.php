  <script type="text/javascript">
  function baseUrl(){
  	return "<?=url('')?>";
  }
  var btns = [
  "pageLength","colvis","csv","excel","pdf","print"
  ];
  </script>
  <script src="<?=url('')?>/assets/plugins/jquery/jquery-2.1.4.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/autocomplete/jquery.autocomplete.js"></script>
  <script src="<?=url('')?>/assets/plugins/pace-master/pace.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/jquery-blockui/jquery.blockui.js"></script>
  <script src="<?=url('')?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/switchery/switchery.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/uniform/jquery.uniform.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/classie/classie.js"></script>
  <script src="<?=url('')?>/assets/plugins/waves/waves.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/3d-bold-navigation/js/main.js"></script>
  <script src="<?=url('')?>/assets/plugins/waypoints/jquery.waypoints.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/jquery-counterup/jquery.counterup.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/toastr/toastr.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/flot/jquery.flot.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/flot/jquery.flot.time.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/flot/jquery.flot.symbol.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/flot/jquery.flot.resize.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/flot/jquery.flot.tooltip.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/datatables/js/jquery.datatables.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/datatables/js/dataTables.buttons.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/datatables/js/buttons.colVis.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/datatables/js/buttons.print.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/datatables/js/buttons.html5.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/curvedlines/curvedLines.js"></script>

  <script src="<?=url('')?>/assets/plugins/datatables/js/dataTables.keyTable.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/datatables/js/dataTables.responsive.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/datatables/js/responsive.bootstrap.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/datatables/js/dataTables.scroller.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/datatables/js/buttons.bootstrap.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/datatables/js/jszip.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/datatables/js/pdfmake.min.js"></script>
  <script src="<?=url('')?>/assets/plugins/datatables/js/vfs_fonts.js"></script>

  <script src="<?=url('')?>/assets/plugins/toastr/toastr.min.js"></script>

  <script src="<?=url('')?>/assets/plugins/metrojs/MetroJs.min.js"></script>
  <script src="<?=url('')?>/assets/js/modern.min.js"></script>

  <script type="text/javascript">


  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-top-center",
    "preventDuplicates": true,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }
  
  </script>
