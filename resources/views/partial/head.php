  
  <!-- Title -->
  <title>Sistem Informasi Rawat Jalan</title>
  
  <meta content="width=device-width, initial-scale=1" name="viewport"/>
  <meta charset="UTF-8">
  <meta name="description" content="Admin Dashboard Template" />
  <meta name="keywords" content="admin,dashboard" />
  <meta name="author" content="Steelcoders" />
  
  <!-- Styles -->
  <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
  <link href="<?=url('')?>/assets/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
  <link href="<?=url('')?>/assets/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
  <link href="<?=url('')?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
  <link href="<?=url('')?>/assets/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
  <link href="<?=url('')?>/assets/plugins/line-icons/simple-line-icons.css" rel="stylesheet" type="text/css"/> 
  <link href="<?=url('')?>/assets/plugins/waves/waves.min.css" rel="stylesheet" type="text/css"/>  
  <link href="<?=url('')?>/assets/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css"/>
  <link href="<?=url('')?>/assets/plugins/3d-bold-navigation/css/style.css" rel="stylesheet" type="text/css"/>
  <link href="<?=url('')?>/assets/plugins/slidepushmenus/css/component.css" rel="stylesheet" type="text/css"/> 
  <link href="<?=url('')?>/assets/plugins/weather-icons-master/css/weather-icons.min.css" rel="stylesheet" type="text/css"/> 
  <link href="<?=url('')?>/assets/plugins/metrojs/MetroJs.min.css" rel="stylesheet" type="text/css"/>  
  <link href="<?=url('')?>/assets/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css"/>
  <link href="<?=url('')?>/assets/plugins/datatables/css/jquery.datatables.min.css" rel="stylesheet" type="text/css"/>  
  <link href="<?=url('')?>/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css"/> 
  <script src="<?=url('')?>/assets/plugins/3d-bold-navigation/js/modernizr.js"></script>
  <link href="<?=url('')?>/assets/plugins/toastr/toastr.min.css" rel="stylesheet"/>
  
  <!-- Theme Styles -->
  <link href="<?=url('')?>/assets/css/modern.min.css" rel="stylesheet" type="text/css"/>
  <link href="<?=url('')?>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
  
  
  
  
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
