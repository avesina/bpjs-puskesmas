<ul class="menu accordion-menu">
  <li class="nav-heading"><span>Navigation</span></li>
  <li><a href="<?=url('')?>"><span class="menu-icon icon-speedometer"></span><p>Dashboard</p></a></li>
  <li><a ><span class="menu-icon icon-speedometer"></span><p>Pendaftaran</p></a>
     <ul class="sub-menu">
      <li><a href="<?=url('pendaftaran/sekarang')?>">Pendaftar (<?=date('Y-m-d')?>)</a></li>
      <li><a href="<?=url('pendaftaran')?>">Semua Data Pendaftar</a></li>
      <li><a href="<?=url('pendaftaran/create')?>">Daftar Baru</a></li>
    </ul>
  </li>
  <li><a href="<?=url('pasien')?>"><span class="menu-icon icon-user"></span><p>Pasien</p></a></li>
  <li><a href="<?=url('pemeriksaan')?>"><span class="menu-icon icon-user"></span><p>Data Kunjungan</p></a></li>
  <li class="droplink"><a href="#"><span class="menu-icon icon-envelope-open"></span><p>Olah Data Kunjungan</p><span class="arrow"></span></a>
    <ul class="sub-menu">
      <?php
        $d = date('d');
        $m = date('m');
        $y = date('Y');
        // dd(date('Y-m-d',strtotime('2016-02-31')));
        if($d<7){
          if($m == 1){
            $start = strtotime(($y-1).'-'.'12-1');
            $end = strtotime(($y-1).'-'.'12-31');
          }else{
            $start = strtotime($y.'-'.($m-1).'-1');
            $end = strtotime($y.'-'.($m-1).'-31');
          }
        }else{
          $start =  strtotime(date('Y-m-1'));
          $end = strtotime(date('Y-m-31'));
        }
        
       ?>
      <li><a href="<?=url('data/rangeumur/'.$start.'/sampai/'.$end)?>">Berdasarkan Umur</a></li>
      <li><a href="<?=url('data/jenispembayaran/'.$start.'/sampai/'.$end)?>">Berdasarkan Jenis Pembayaran</a></li>
      <li><a href="<?=url('data/jeniskunjungan/'.$start.'/sampai/'.$end)?>">Berdasarkan Jenis Kunjungan</a></li>
      <li><a href="<?=url('data/jeniskasus/'.$start.'/sampai/'.$end)?>">Berdasarkan Jenis Kasus</a></li>
    </ul>
  </li>
  <li class="droplink"><a href="#"><span class="menu-icon icon-grid"></span><p>Pola Penyakit</p><span class="arrow"></span></a>
    <ul class="sub-menu">
      <li><a href="<?=url('data/penyakit/'.$start.'/sampai/'.$end)?>">Semua Penyakit</a></li>
      <li><a href="<?=url('data/penyakit/chart/'.$start.'/sampai/'.$end)?>">Grafik Pola Penyakit</a></li>
    </ul>
  </li>
  <li class="droplink"><a href="#"><span class="menu-icon icon-note"></span><p>Management Master Data</p><span class="arrow"></span></a>
    <ul class="sub-menu">
      <li><a href="<?=url('kabupaten')?>">Data Kabupaten</a></li>
      <li><a href="<?=url('kecamatan')?>">Data Kecamatan</a></li>
      <li><a href="<?=url('kelurahan')?>">Data Kelurahan</a></li>
      <li><a href="<?=url('diagnosis')?>">Diagnosis</a></li>
      <li><a href="<?=url('kabupaten')?>">Users</a></li>
    </ul>
  </li>
  <li><a href="<?=url('import')?>"><span class="menu-icon icon-user"></span><p>Import Data</p></a></li>
</ul>