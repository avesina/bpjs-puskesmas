<script type="text/javascript">
var handleDataTableButtons = function() {
	"use strict";
	var targets = [];
	if(0 !== $(".datatable-responsive").length){
		var vis = $('.datatable-responsive').data('visible');
		if(typeof vis !== typeof undefined && vis !== false){
			targets = vis.split(',').map(Number);
		}
		var btns = [
		{
			extend:"pageLength",
			className: "btn-sm btn-warning"
		},
		{
			extend:"colvis",
			className: "btn-sm btn-info"
		},
		{
			extend: "csv",
			className: "btn-sm"
		}, {
			extend: "excel",
			className: "btn-sm"
		}, {
			extend: "pdf",
			className: "btn-sm"
		}, {
			extend: "print",
			className: "btn-sm"
		}
		];
		var btn_vis = $('.datatable-responsive').attr('btn-invisible');
		if(typeof btn_vis !== typeof undefined && btn_vis !== false){
			btns = [];
		}
	}

	0 !== $(".datatable-responsive").length && $('.datatable-responsive').DataTable({
		dom: "Bfrtip",
		buttons: btns,
		"columnDefs": [
		{
			"targets": targets,
			"visible": false
		}
		],
		responsive: true
	})
},
TableManageButtons = function() {
	"use strict";
	return {
		init: function() {
			handleDataTableButtons()
		}
	}
}();
</script>
<script type="text/javascript">
// PNotify.prototype.options.delay = 2500;
// window.alert = function(message) {
// 	var res = message.split(",");
// 	console.log(message);
// 	var type = res[0];
// 	var title = res[1];
// 	var text = res[2];
// 	new PNotify({
// 		type : type,
// 		title: title,
// 		text: text,
// 		buttons: {
// 			sticker: false
// 		}
// 	});
// };

TableManageButtons.init();
function deleteData(ini){
	var url = $(ini).data('url');
	var direct = $(ini).data('direct');
	var r = confirm("Apakah anda yakin akan delete data?");
	if (r == true) {
		txt = "You pressed OK!";
		$.ajax({
			type: "DELETE",
			url: url,
			success: function(data){
				if(data.status==true){
					$.each( data.message, function( key, value ) {
						alert('success,Alert,'+value);
					});
					if(typeof direct !== typeof undefined && direct !== false){
						window.location.href=direct; 
					}
				}else{
					$.each( data.message, function( key, value ) {
						var interval = setInterval(function(){
							alert('error,Alert,'+value);
							clearInterval(interval);
						},2000);

					});

				}
			}
		});
	} else {
		txt = "You pressed Cancel!";
	}
}

$('#autocomplete-custom-append').autocomplete({
	serviceUrl:'<?=url("autocomplete/pasien")?>',
	dataType:'json',
	appendTo: '#autocomplete-container',
	onSelect: function (suggestion) {
		$(this).val('');
		$('#no_rekam_medis').val(suggestion.data.no_rekam_medis);
		$('#no_rekam_medis_lama').val(suggestion.data.no_rekam_medis_lama);
		$('#nobpjs').val(suggestion.data.nobpjs);
		$('#nama_lengkap').val(suggestion.data.nama_lengkap);
		$('#tgl_lahir').val(suggestion.data.tgl_lahir);
		$('#id_pasien').val(suggestion.data.id);
		$('#jeniskunjungan').val(suggestion.jenis_kunjungan);
	}
});

$('#select_kab').change(function(){
	if($(this).val()==''){
		$('#select_kec').prop('disabled',true);
	}else{
		$('#select_kec').html('');
		$.ajax({
			type: 'POST',
			url: '<?=url("kecamatan/select")?>',
			data: {id_kabupaten:$(this).val()},
			success: function(data){
				$('#select_kec').append('<option></option>');
				$.each(data,function(key,value){
					$('#select_kec').append('<option value="'+value.id+'">'+value.nama_kecamatan+'</option>')
				});
				$('#select_kec').select2({
					placeholder: "Select a state",
					allowClear: true
				});
			}
		});
	}
});

$('#select_kec').change(function(){
	if($(this).val()==''){
		$('#select_kel').prop('disabled',true);
	}else{
		$('#select_kel').html('');
		$.ajax({
			type: 'POST',
			url: '<?=url("kelurahan/select")?>',
			data: {id_kecamatan:$(this).val()},
			success: function(data){
				$('#select_kel').append('<option></option>');
				$.each(data,function(key,value){
					$('#select_kel').append('<option value="'+value.id+'">'+value.nama_kelurahan+'</option>')
				});
				$('#select_kel').select2({
					placeholder: "Select a state",
					allowClear: true
				});
			}
		});
	}
});

$(".ajaxsubmit").submit(function(e) {
	e.preventDefault(e);
	var url = $(this).attr('action');
	var type = $(this).attr('method');
	var buttonSubmit = $(this).find('button[type=submit]');
	var direct = $(this).attr('direct');
	buttonSubmit.prop('disabled', true);
	$.ajax({
		type: type,
		url: url,
		data: $(this).serialize(),
		success: function(data)
		{
			
			if(data.status==true){
				$.each( data.message, function( key, value ) {
					// alert('success,Alert,'+value);
					toastr["success"](value);
				});
				if(typeof direct !== typeof undefined && direct !== false){
					window.location.href=direct; 
				}
			}else{
				$.each( data.message, function( key, value ) {
					setTimeout(function() {
						toastr["error"](value);
					}, key * 700); 
				});

			}
			var interval = setInterval(function()
			{ 
				buttonSubmit.prop('disabled', false); 
				clearInterval(interval);
			}, 3000);
		},
		error: function(data){
			// alert('success,Alert,something wrong happen');
			toastr["error"]('Terjadi kesalahan');
			var interval = setInterval(function(){ 
				buttonSubmit.prop('disabled', false); 
				clearInterval(interval);
			}, 3000);
			
		}
	});
});
</script>
<script type="text/javascript">

$('.datatable-diagnosis').DataTable({
	"processing": true,
	"serverSide": true,
	"ajax": {
		"url": "<?=url('diagnosis/list')?>",
		"type": "POST"
	},
	"columns": [
	{ "data": "id" },
	{ "data": "kode"},
	{ "data": "nama_diagnosis" }
	]

});
$('#modal_diagnosis tbody').on('click', 'tr', function () {
	var id = $(this).children('td:first').text();
	var nama = $(this).children('td:last').text();
	$('#nama_diagnosis').val(nama);
	$('#id_diagnosis').val(id);
	$('#modal_diagnosis').modal('hide');
});
</script>