  <div class="page-title">
    <div class="container">
      <h3>Dashboard</h3>
    </div>
  </div>
  <div id="main-wrapper" class="container">
    <div class="row">
      <div class="col-lg-3 col-md-6">
        <div class="panel info-box panel-white">
          <div class="panel-body">
            <div class="info-box-stats">
              <p class="tgl-counter"></p>
              <span class="info-box-title">Kunjungan Tanggal <?=date('Y-m-d')?></span>
            </div>
            <div class="info-box-icon">
              <i class="icon-users"></i>
            </div>
            <div class="info-box-progress">
              <div class="progress progress-xs progress-squared bs-n">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="panel info-box panel-white">
          <div class="panel-body">
            <div class="info-box-stats">
              <p class="monthly-counter"></p>
              <span class="info-box-title">Kunjungan Bulan <?=date('M')?></span>
            </div>
            <div class="info-box-icon">
              <i class="icon-eye"></i>
            </div>
            <div class="info-box-progress">
              <div class="progress progress-xs progress-squared bs-n">
                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="panel info-box panel-white">
          <div class="panel-body">
            <div class="info-box-stats">
              <p><span class="yearly-counter"></span></p>
              <span class="info-box-title">Kunjungan tahun <?=date('Y')?></span>
            </div>
            <div class="info-box-icon">
              <i class="icon-basket"></i>
            </div>
            <div class="info-box-progress">
              <div class="progress progress-xs progress-squared bs-n">
                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="panel info-box panel-white">
          <div class="panel-body">
            <div class="info-box-stats">
              <p class="total-counter"></p>
              <span class="info-box-title">Total Kunjungan</span>
            </div>
            <div class="info-box-icon">
              <i class="icon-envelope"></i>
            </div>
            <div class="info-box-progress">
              <div class="progress progress-xs progress-squared bs-n">
                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!-- Row -->
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
          <div class="row">
            <div class="col-sm-9">
              <div class="visitors-chart">
                <div class="panel-heading">
                  <h4 class="panel-title">Pengunjung Perhari</h4>
                </div>
                <div class="panel-body">
                  <div id="flotchart1"></div>
                </div>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="stats-info">
                <div class="panel-heading">
                  <h4 class="panel-title">10 Diagnosis Bulan <?=date('M')?></h4>
                </div>
                <div class="panel-body">
                  <ul class="list-unstyled list-pain">

                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
          <div class="panel-heading">
            <h4 class="panel-title">Pasien Hari Ini</h4>
          </div>
          <div class="panel-body">
            <div class="table-responsive project-stats">  
             <table class="table">
               <thead>
                 <tr>
                   <th>#</th>
                   <th>Project</th>
                   <th>Status</th>
                   <th>Manager</th>
                   <th>Progress</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row">452</th>
                   <td>Mailbox Template</td>
                   <td><span class="label label-info">Pending</span></td>
                   <td>David Green</td>
                   <td>
                     <div class="progress progress-sm">
                       <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                       </div>
                     </div>
                   </td>
                 </tr>
                 <tr>
                   <th scope="row">327</th>
                   <td>Wordpress Theme</td>
                   <td><span class="label label-primary">In Progress</span></td>
                   <td>Sandra Smith</td>
                   <td>
                     <div class="progress progress-sm">
                       <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                       </div>
                     </div>
                   </td>
                 </tr>
                 <tr>
                   <th scope="row">226</th>
                   <td>Modern Admin Template</td>
                   <td><span class="label label-success">Finished</span></td>
                   <td>Chritopher Palmer</td>
                   <td>
                     <div class="progress progress-sm">
                       <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                       </div>
                     </div>
                   </td>
                 </tr>
                 <tr>
                   <th scope="row">178</th>
                   <td>eCommerce template</td>
                   <td><span class="label label-danger">Canceled</span></td>
                   <td>Amily Lee</td>
                   <td>
                     <div class="progress progress-sm">
                       <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                       </div>
                     </div>
                   </td>
                 </tr>
                 <tr>
                   <th scope="row">157</th>
                   <td>Website PSD</td>
                   <td><span class="label label-info">Testing</span></td>
                   <td>Nick Doe</td>
                   <td>
                     <div class="progress progress-sm">
                       <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
                       </div>
                     </div>
                   </td>
                 </tr>
                 <tr>
                   <th scope="row">157</th>
                   <td>Fronted Theme</td>
                   <td><span class="label label-warning">Waiting</span></td>
                   <td>David Green</td>
                   <td>
                     <div class="progress progress-sm">
                       <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                       </div>
                     </div>
                   </td>
                 </tr>
               </tbody>
             </table>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
 <?=partial('main_js')?>
 <script src="<?=url('')?>/assets/plugins/flot/jquery.flot.min.js"></script>
 <script src="<?=url('')?>/assets/plugins/flot/jquery.flot.time.min.js"></script>
 <script src="<?=url('')?>/assets/plugins/flot/jquery.flot.symbol.min.js"></script>
 <script src="<?=url('')?>/assets/plugins/flot/jquery.flot.resize.min.js"></script>
 <script src="<?=url('')?>/assets/plugins/flot/jquery.flot.tooltip.min.js"></script>
 <script src="<?=url('')?>/assets/plugins/flot/jquery.flot.pie.min.js"></script>
 <script type="text/javascript">
 $(document).ready(function(){
  var flot1 = function (data) {
    var dataset =  [
    {
      data: data,
      color: "rgba(34,186,160,1)",
      lines: {
        show: true,
        fill: 0.2,
      },
      shadowSize: 0,
    },{
      data: data,
      color: "#fff",
      lines: {
        show: false,
      },
      curvedLines: {
        apply: false,
      },
      points: {
        show: true,
        fill: true,
        radius: 4,
        fillColor: "rgba(34,186,160,1)",
        lineWidth: 2
      },
      shadowSize: 0
    }
    ];

    var ticks = [[0, "1"], [1, "2"], [2, "3"], [3, "4"], [4, "5"], [5, "6"], [6, "7"], [7, "8"]];

    var plot1 = $.plot("#flotchart1", dataset, {
      series: {
        color: "#14D1BD",
        lines: {
          show: true,
          fill: 0.2
        },
        shadowSize: 0,
        curvedLines: {
          apply: true,
          active: true
        }
      },
      xaxis: {
        mode: "time"
      },
      legend: {
        show: false
      },
      grid: {
        color: "#AFAFAF",
        hoverable: true,
        borderWidth: 0,
        backgroundColor: '#FFF'
      }
    });

  };
  // $.get(baseUrl()+'/dashboard/grafik',function(data){
  //   flot1(data);
  // });
  

  $( window ).load(function() {
    setTimeout(function(){
      $.get(baseUrl()+'/dashboard',function(data){
        $('.total-counter').text(data.total);
        $('.yearly-counter').text(data.year);
        $('.monthly-counter').text(data.monthly);
        $('.tgl-counter').text(data.today);
        $('.listpain').empty();
        $.each(data.listpain,function(key,value){
          $('.list-pain').append('<li>'+value.nama_diagnosis+'<div class="text-success pull-right">'+(value.jumlah/data.total)*100+'%<i class="fa fa-level-up"></i></div></li>')
        });
        flot1(data.data);
      });

    },2000)
  }           );
  });

  </script>