 <div class="page-title">
 	<div class="container">
 		<h3>Tabel Pasien</h3>
 	</div>
 </div>
 <div id="main-wrapper" class="container">
 	<div class='row'>
 		<div class="col-md-12 col-sm-12 col-xs-12">
 			<div class="panel panel-white">
 				<div class="panel-heading clearfix">
 					<a href="<?=url('pasien/create')?>" type="button" class="btn btn-success m-b-sm" >Tambah Pasien</a>
 				</div> 	
 				<div class="panel-body">
 					<p class="text-muted font-13 m-b-30">
 						Responsive is an extension for DataTables that resolves that problem by optimising the table's layout for different screen sizes through the dynamic insertion and removal of columns from the table.
 					</p>
 					<table class="datatable-pasien table table-striped table-bordered dt-responsive nowrap" data-visible ="2,3,4,5,6,10,11" cellspacing="0" width="100%">
 						<thead>
 							<tr>
 								<th>no rekam medis</th>
 								<th>no bpjs</th>
 								<th>kdfaskes</th>
 								<th>nama faskes</th>
 								<th>noka</th>
 								<th>no KK</th>
 								<th>no SIN</th>
 								<th>Nama Lengkap</th>
 								<th>Tanggal lahir</th>
 								<th>Umur</th>
 								<th>PISA</th>
 								<th>Alamat</th>
 								<th>Kab</th>
 								<th>Kec</th>
 								<th>Kel</th>
 								<th>action</th>
 							</tr>
 						</thead>
 						<tbody>
 						</tbody>
 					</table>

 				</div>
 			</div>
 		</div>
 	</div>
 </div>
 <?=partial('main_js')?>
 <?=partial('js')?>
 <script type="text/javascript">
 $('.datatable-pasien').DataTable({
 	"dom": "Bfrtip",
	"buttons": btns,
	"responsive": true,
 	"processing": true,
 	"serverSide": true,
 	"ajax": {
 		"url": "<?=url('pasien/list')?>",
 		"type": "POST"
 	},
 	"columns": [
 	{ "data": "no_rekam_medis" },
 	{ "data": "nobpjs"},
 	{ "data": "kdfaskes" },
 	{ "data": "namafaskes" },
 	{ "data": "noka" },
 	{ "data": "nokk" },
 	{ "data": "nosin" },
 	{ "data": "nama_lengkap" },
 	{ "data": "tgl_lahir" },
 	{ "data": "umur" },
 	{ "data": "pisa" },
 	{ "data": "alamat" },
 	{ "data": "kabupaten" },
 	{ "data": "kecamatan" },
 	{ "data": "kelurahan" },
 	{ "data": null}
 	],
 	"columnDefs": [
		{
			"targets": [1,2,3,4,5,6,10,11],
			"visible": false
		}
	],
 	rowCallback: function(row,data,index){
 		var btn_edit = '<a class="btn btn-info" href="'+baseUrl()+'/pasien/'+data.id+'/edit"><i class="fa fa-edit"></i></a>';
 		var btn_delete = '<button onclick="deleteData(this)" data-url="'+baseUrl()+'/pasien/'+data.id+'" data-direct="'+baseUrl()+'/pasien" class="btn btn-danger" href="#/pencil-square-o"><i class="fa fa-trash"></i></button>'
 		$('td:last-child', row).html( btn_edit+btn_delete );
 	}


 });
 </script>