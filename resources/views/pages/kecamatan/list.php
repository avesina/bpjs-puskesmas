 <div class="page-title">
 	<div class="container">
 		<h3>Tabel Kecamatan</h3>
 	</div>
 </div>
 <div id="main-wrapper" class="container">
 	<div class='row'>
 		<div class="col-md-12 col-sm-12 col-xs-12">
 			<div class="panel panel-white">
 				<div class="panel-heading clearfix">
 					<a href="<?=url('kecamatan/create')?>" type="button" class="btn btn-success m-b-sm" >Tambah Kecamatan</a>
 				</div> 	
 				<div class="panel-body">
 					<p class="text-muted font-13 m-b-30">
 						Responsive is an extension for DataTables that resolves that problem by optimising the table's layout for different screen sizes through the dynamic insertion and removal of columns from the table.
 					</p>
				<table class="datatable-responsive table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>id</th>
							<th>Kabupaten</th>
							<th>Nama Kecamatan</th>
							<th width='80px'></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($data as $key => $value): ?>
						<tr>
							<td><?=$value->id?></td>
							<th><?=$value->kabupaten()->first()->nama_kabupaten?></th>
							<td><?=$value->nama_kecamatan?></td>
							<td>
								<a class="btn btn-info" href="<?=url('kecamatan/'.$value->id.'/edit')?>"><i class="fa fa-edit"></i></a>
								<button onclick="deleteData(this)" data-url="<?=url('kecamatan/'.$value->id)?>" data-direct="<?=url('/kabupaten')?>" class="btn btn-danger" href="#/pencil-square-o"><i class="fa fa-trash"></i></button> 
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>

			</div>
		</div>
	</div>
</div>
</div>
 <?=partial('main_js')?>
 <?=partial('js')?>