 <div class="page-title">
 	<div class="container">
 		<h3>Tabel Kunjungan</h3>
 	</div>
 </div>
 <div id="main-wrapper" class="container">
 	<div class='row'>
 		<div class="col-md-12 col-sm-12 col-xs-12">
 			<div class="panel panel-white">
 				<div class="panel-body">
 					<table class="datatable-kunjungan table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
 						<thead>
 							<tr>
 								<th>Tanggal</th>
 								<th>Nama Lengkap</th>
 								<th>Umur</th>
 								<th>Jenis Pembayaran</th>
 								<th>Jenis Pelayanan</th>
 								<th>Jenis Kunjungan</th>
 								<th>Keluhan</th>
 								<th>Diagnosis</th>
 								<th>Action</th>
 							</tr>
 						</thead>
 					
 					</tbody>
 				</table>
 			</div>
 		</div>
 	</div>
 </div>
</div>
 <?=partial('main_js')?>
 <?=partial('js')?>
 <script type="text/javascript">
 $('.datatable-kunjungan').DataTable({
 	"dom": "Bfrtip",
	"buttons": btns,
	"responsive": true,
 	"processing": true,
 	"serverSide": true,
 	"ajax": {
 		"url": "<?=url('pemeriksaan/list')?>",
 		"type": "POST"
 	},
 	"columns": [
 	{ "data": "tanggal" },
 	{ "data": "nama_lengkap" },
 	{ "data": "umur"},
 	{ "data": "jenis_pembayaran"},
 	{ "data": "jenis_pelayanan"},
 	{ "data": "jenis_kunjungan"},
 	{ "data": "keluhan"},
 	{ "data": "diagnosis"},
 	{ "data": null}
 	],
 	"columnDefs": [
		{
			"targets": [6],
			"visible": false
		}
	],
 	rowCallback: function(row,data,index){
 		var btn_edit = '<a class="btn btn-info" href="'+baseUrl()+'/pasien/'+data.id+'/edit"><i class="fa fa-edit"></i></a>';
 		var btn_delete = '<button onclick="deleteData(this)" data-url="'+baseUrl()+'/pasien/'+data.id+'" data-direct="'+baseUrl()+'/pasien" class="btn btn-danger" href="#/pencil-square-o"><i class="fa fa-trash"></i></button>'
 		$('td:last-child', row).html( btn_edit+btn_delete );
 	}


 });
 </script>