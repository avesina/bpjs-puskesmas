 <div class="page-title">
 	<div class="container">
 		<h3>Tabel Pasien</h3>
 	</div>
 </div>
 <div id="main-wrapper" class="container">
 	<div class='row'>
 		<div class="col-md-12 col-sm-12 col-xs-12">
 			<div class="panel panel-white">
 				<div class="panel-heading clearfix">
 					<a href="<?=url('pendaftaran/create')?>" type="button" class="btn btn-success m-b-sm" >Daftar Baru</a>
 				</div> 	
 				<div class="panel-body">
 					<p class="text-muted font-13 m-b-30">
 						Responsive is an extension for DataTables that resolves that problem by optimising the table's layout for different screen sizes through the dynamic insertion and removal of columns from the table.
 					</p>
 					<table class="datatable-pendaftaran table table-striped table-bordered dt-responsive nowrap" data-visible ="2,3,4,5,6,10,11" cellspacing="0" width="100%">
 							<thead>
						<tr>
							<th>ID</th>
							<th>no Rekam Medis</th>
							<th>no BPJS</th>
							<th>Nama Lengkap</th>
							<th>Jenis Pembayaran</th>
							<th>Jenis Pelayanan</th>
							<th>Jenis Kunjungan</th>
							<th>Keluhan</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($data as $key => $value): ?>
						<tr>
							<td><?=$value->id?></td>
							<td><a href="<?=url('pemeriksaan/'.$value->id)?>"><?=($value->pasien->no_rekam_medis)?$value->pasien->no_rekam_medis:'klik disini'?></td>
							<td><?=$value->pasien->nobpjs?></td>
							<td><?=$value->pasien->nama_lengkap?></td>
							<td><?=$value->jenis_pembayaran?></td>
							<td><?=$value->pelayanan->nama_layanan?></td>
							<td><?=$value->jenis_kunjungan?></td>
							<td><?=$value->keluhan?></td>
						</tr>
						<?php endforeach; ?>
					</tbody>
 					</table>

 				</div>
 			</div>
 		</div>
 	</div>
 </div>
 <?=partial('main_js')?>
 <?=partial('js')?>
 <script type="text/javascript">
 $('.datatable-pendaftaran').DataTable({
 	"dom": "Bfrtip",
	"buttons": btns,
	"responsive": true,
 	rowCallback: function(row,data,index){
 		console.log(data);
 		var btn_edit = '<a class="btn btn-info" href="'+baseUrl()+'/pendaftaran/'+data[0]+'/edit"><i class="fa fa-edit"></i></a>';
 		var btn_delete = '<button onclick="deleteData(this)" data-url="'+baseUrl()+'/pendaftaran/'+data[0]+'" data-direct="'+baseUrl()+'/pasien" class="btn btn-danger" href="#/pencil-square-o"><i class="fa fa-trash"></i></button>'
 		$('td:last-child', row).html( btn_edit+btn_delete );
 	}


 });
 </script>