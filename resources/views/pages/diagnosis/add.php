 <?php
 if(isset($diagnosis)){
 	$id = $diagnosis->id;
 	$nama_diagnosis = $diagnosis->nama_diagnosis;
 	$keterangan = $diagnosis->keterangan;
 	$method = "PUT";
 	$action = url('diagnosis/'.$id);
 }else{
 	$method = "POST";
 	$action = url('diagnosis');
 	$id = '';
 	$nama_diagnosis = '';
 	$keterangan = '';
 }
 ?>
 <div id="main-wrapper" class="container">
 	<div class='row'>
 		<div class="col-md-12 col-sm-12 col-xs-12">
 			<div class="panel panel-white">
 				<div class="panel-body">
 				<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left ajaxsubmit" method="<?=$method?>" direct ='<?=url('diagnosis')?>' action="<?=$action?>">
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Diagnosis
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<input name="nama_diagnosis" value="<?=$nama_diagnosis?>" type="text" class="form-control col-md-7 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Keterangan
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<textarea name="keterangan" value="<?=$keterangan?>" type="text" class="form-control col-md-7 col-xs-12"><?=$keterangan?></textarea>
 						</div>
 					</div>
 					<div class="ln_solid"></div>
 					<div class="form-group">
 						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
 							<button type="reset" class="btn btn-primary">Cancel</button>
 							<button type="submit" class="btn btn-success">Submit</button>
 						</div>
 					</div>
 				</form>
 			</div>
 		</div>
 	</div>
 </div>
</div>
 <?=partial('main_js')?>
 <?=partial('js')?>