 <div class="page-title">
 	<div class="container">
 		<h3>Tabel Kabupaten</h3>
 	</div>
 </div>
 <div id="main-wrapper" class="container">
 	<div class='row'>
 		<div class="col-md-12 col-sm-12 col-xs-12">
 			<div class="panel panel-white">
 				<div class="panel-heading clearfix">
 					<a class="btn btn-default" href="<?=url('kabupaten/create')?>"><i class="fa fa-plus"></i> Kabupaten</a></i></a>
 				</div>
 				<div class="panel-body">
 					<table class="datatable-responsive table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
 						<thead>
 							<tr>
 								<th>id</th>
 								<th>Nama Kabupaten</th>
 								<th></th>
 							</tr>
 						</thead>
 						<tbody>
 							<?php foreach ($data as $key => $value): ?>
 							<tr>
 								<td><?=$value->id?></td>
 								<td><?=$value->nama_kabupaten?></td>
 								<td>
 									<a class="btn btn-info" href="<?=url('kabupaten/'.$value->id.'/edit')?>"><i class="fa fa-edit"></i></a>
 									<button onclick="deleteData(this)" data-url="<?=url('kabupaten/'.$value->id)?>" data-direct="<?=url('/kabupaten')?>" class="btn btn-danger" href="#/pencil-square-o"><i class="fa fa-trash"></i></button> 
 								</td>
 							</tr>
 						<?php endforeach; ?>
 					</tbody>
 				</table>

 			</div>
 		</div>
 	</div>
 </div>
</div>
 <?=partial('main_js')?>
 <?=partial('js')?>