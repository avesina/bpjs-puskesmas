 <?php
 if(isset($kelurahan)){
 	$id = $kelurahan->id;
 	$id_kecamatan = $kelurahan->id_kecamatan;
 	$nama_kelurahan = $kelurahan->nama_kelurahan;
 	$method = "PUT";
 	$action = url('kelurahan/'.$id);
 }else{
 	$method = "POST";
 	$action = url('kelurahan');
 	$id = '';
 	$id_kecamatan = '';
 	$nama_kelurahan = '';
 }
 ?>
 <div id="main-wrapper" class="container">
 	<div class='row'>
 		<div class="col-md-12 col-sm-12 col-xs-12">
 			<div class="panel panel-white">
 				<div class="panel-body">
 				<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left ajaxsubmit" method="<?=$method?>" direct ='<?=url('kelurahan')?>' action="<?=$action?>">
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Kelurahan
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<input name="nama_kelurahan" value="<?=$nama_kelurahan?>" type="text" class="form-control col-md-7 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Terletak di Kecamatan
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<select name="id_kecamatan" id="select_kel" class="form-control">
 								<option></option>
 								<?php foreach (kecamatan() as $key => $value) {
 									$select = "";
 									if($value->id == $id_kecamatan){
 										$select="selected";
 									}
 									echo "<option ".$select." value='".$value->id."' >".$value->nama_kecamatan."</option>";
 								} ?>
 							</select>
 						</div>
 					</div>
 					<div class="ln_solid"></div>
 					<div class="form-group">
 						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
 							<button type="reset" class="btn btn-primary">Cancel</button>
 							<button type="submit" class="btn btn-success">Submit</button>
 						</div>
 					</div>
 				</form>
 			</div>
 		</div>
 	</div>
 </div>
</div>
 <?=partial('main_js')?>
 <?=partial('js')?>