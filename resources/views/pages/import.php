 <div class="page-title">
 	<div class="container">
 		<h3>Tabel Kunjungan</h3>
 	</div>
 </div>
 <div id="main-wrapper" class="container">
 <div class="row">
 	<div class="col-md-6 col-sm-6 col-xs-6">
 		<div class="panel panel-white">
 			<div class="panel-heading clearfix">
 				Import Data Pasien
 			</div>
 			<div class="panel-body">
 				<br />
 				<form enctype="multipart/form-data" method="POST" action="<?=url('import/pasien')?>" class="form-horizontal form-label-left">
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">file 
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-6">
 							<input accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" class="form-control col-md-7 col-xs-12" name="excel" type="file"></input>
 						</div>
 					</div>
 					<div class="ln_solid"></div>
 					<div class="form-group">
 						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
 							<button type="reset" class="btn btn-primary">Cancel</button>
 							<button type="submit" class="btn btn-success">Submit</button>
 						</div>
 					</div>
 				</form>
 			</div>
 		</div>
 	</div>
 	<div class="col-md-6 col-sm-6 col-xs-6">
 		<div class="panel panel-white">
 			<div class="panel-heading clearfix">
 				Import Data Diagnosis
 			</div>
 			<div class="panel-body">
 				<br />
 				<form enctype="multipart/form-data" method="POST" action="<?=url('import/diagnosis')?>" class="form-horizontal form-label-left">
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">file 
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-6">
 							<input accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" class="form-control col-md-7 col-xs-12" name="excel" type="file"></input>
 						</div>
 					</div>
 					<div class="ln_solid"></div>
 					<div class="form-group">
 						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
 							<button type="reset" class="btn btn-primary">Cancel</button>
 							<button type="submit" class="btn btn-success">Submit</button>
 						</div>
 					</div>
 				</form>
 			</div>
 		</div>
 	</div>
 </div>
</div>