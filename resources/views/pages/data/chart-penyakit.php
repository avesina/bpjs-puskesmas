 <div class="page-title">
 	<div class="container">
 		<h3>Tabel Berdasarkan Umur</h3>
 	</div>
 </div>
 <div id="main-wrapper" class="container">
 	<div class='row'>
 		<div class="col-md-12 col-sm-12 col-xs-12">
 			<div class="panel panel-white">
 				<div class="panel-heading clearfix">
 					<div class="form-inline col-md-11">
 						<div class="input-group input-daterange form-group">
 							<input id="input_start" type="text" class="form-control" value="<?=date("Y-m-d",$start)?>">
 							<span class="input-group-addon"> sampai </span>
 							<input id="input_end" type="text" class="form-control" value="<?=date("Y-m-d",$end)?>">

 						</div>
 						<a id="input_see" data-url="<?=url('data/rangeumur')?>" href="" class="btn btn-info"> lihat </a>
 					</div>

 				</div>
 				<div class="panel-body">
 					<div class="panel-body">
 						<div class="tabs-left" role="tabpanel">
 							<!-- Nav tabs -->
 							<ul class="nav nav-tabs" role="tablist">
 								<li role="presentation" class="active"><a href="#tab9" role="tab" data-toggle="tab">Semua Umur</a></li>
 								<li role="presentation"><a href="#tab1" role="tab" data-toggle="tab">0 - 7 hari</a></li>
 								<li role="presentation"><a href="#tab2" role="tab" data-toggle="tab">8 - 30 hari</a></li>
 								<li role="presentation"><a href="#tab3" role="tab" data-toggle="tab">1 - 11 bulan</a></li>
 								<li role="presentation"><a href="#tab4" role="tab" data-toggle="tab">1 - 4 tahun</a></li>
 								<li role="presentation"><a href="#tab5" role="tab" data-toggle="tab">5 - 14 tahun</a></li>
 								<li role="presentation"><a href="#tab6" role="tab" data-toggle="tab">15 - 44 tahun</a></li>
 								<li role="presentation"><a href="#tab7" role="tab" data-toggle="tab">45 - 64 tahun</a></li>
 								<li role="presentation"><a href="#tab8" role="tab" data-toggle="tab">64 tahun lebih</a></li>
 							</ul>
 							<style type="text/css">
 							.flot{
 								height: 200px
 							}
 							</style>
 							<div class="tab-content">
 								<div role="tabpanel" class="tab-pane active fade in" id="tab9">
 									<p>Data Semua Umur</p>
 									<div class="panel-body col-md-2">
 									</div>
 									<div class="panel-body col-md-10">
 										<div class="flot"></div>
 									</div>

 									


 								</div>
 								<div role="tabpanel" class="tab-pane fade" id="tab1">
 									<p>Data 0 - 7 hari</p>
 									<div class="panel-body col-md-2">
 									</div>
 									<div class="panel-body col-md-10">
 										<div class="flot"></div>
 									</div>


 								</div>
 								<div role="tabpanel" class="tab-pane fade" id="tab2">
 									<p>Data 8 - 30 hari</p>
 									<div class="panel-body col-md-2">
 									</div>
 									<div class="panel-body col-md-10">
 										<div class="flot"></div>
 									</div>
 								</div>
 								<div role="tabpanel" class="tab-pane fade" id="tab3">
 									<p>Data 1 - 11 bulan</p>
 									<div class="panel-body col-md-2">
 									</div>
 									<div class="panel-body col-md-10">
 										<div class="flot"></div>
 									</div>
 								</div>
 								<div role="tabpanel" class="tab-pane fade" id="tab4">
 									<p>Data 1 - 4 tahun</p>
 									<div class="panel-body col-md-2">
 									</div>
 									<div class="panel-body col-md-10">
 										<div class="flot"></div>
 									</div>
 								</div>
 								<div role="tabpanel" class="tab-pane fade" id="tab5">
 									<p>Data 5 - 14 tahun</p>
 									<div class="panel-body col-md-2">
 									</div>
 									<div class="panel-body col-md-10">
 										<div class="flot"></div>
 									</div>
 								</div>
 								<div role="tabpanel" class="tab-pane fade" id="tab6">
 									<p>Data 15 - 44 tahun</p>
 									<div class="panel-body col-md-2">
 									</div>
 									<div class="panel-body col-md-10">
 										<div class="flot"></div>
 									</div>
 								</div>
 								<div role="tabpanel" class="tab-pane fade" id="tab7">
 									<p>Data 45 - 64 tahun</p>
 									<div class="panel-body col-md-2">
 									</div>
 									<div class="panel-body col-md-10">
 										<div class="flot"></div>
 									</div>
 								</div>
 								<div role="tabpanel" class="tab-pane fade" id="tab8">
 									<p>Data 64 tahun lebih</p>
 									<div class="panel-body col-md-2">
 									</div>
 									<div class="panel-body col-md-10">
 										<div class="flot"></div>
 									</div>
 								</div>
 							</div>
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>
 	</div>
 </div>
 <?=partial('main_js')?>
 <script src="<?=url('')?>/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
 <script src="<?=url('')?>/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
 <script src="<?=url('')?>/assets/plugins/flot/jquery.flot.min.js"></script>
 <script src="<?=url('')?>/assets/plugins/flot/jquery.flot.time.min.js"></script>
 <script src="<?=url('')?>/assets/plugins/flot/jquery.flot.symbol.min.js"></script>
 <script src="<?=url('')?>/assets/plugins/flot/jquery.flot.resize.min.js"></script>
 <script src="<?=url('')?>/assets/plugins/flot/jquery.flot.tooltip.min.js"></script>
 <script src="<?=url('')?>/assets/plugins/flot/jquery.flot.pie.min.js"></script>
 <script type="text/javascript">
 $('.input-daterange input').each(function() {
 	$(this).datepicker({
 		orientation: "top auto",
 		format: 'yyyy-mm-dd',
 		autoclose: true
 	});
 });
 </script>
 <script type="text/javascript">
 $(document).ready(function(){
 	$.get(window.location.href+'/json?tab=9',function(data){
 		var ini = $("#tab9").find('.flot');
 		flot1(ini,data.data,data.ticks);
 	});
 	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	  var target = $(e.target).attr("href") // activated tab
	  // alert(target);
	  $.get(window.location.href+'/json?tab='+target.replace('#tab',''),function(data){
	  	var ini = $(target).find('.flot');
	  	flot1(ini,data.data,data.ticks);
	  });
	});
 	var flot1 = function (ini,data,ticks) {
 	//var data = [[0, 11], [1, 15], [2, 25], [3, 24], [4, 13], [5, 18]];
 	var dataset = [{
 		data: data,
 		color: "#22BAA0"
 	}];
 	//var ticks = [[0, "1"], [1, "2"], [2, "3"], [3, "4"], [4, "5"], [5, "6"]];

 	var options = {
 		series: {
 			bars: {
 				show: true
 			}
 		},
 		bars: {
 			align: "center",
 			barWidth: 0.5
 		},
 		xaxis: {
 			ticks: ticks
 		},
 		yaxis: {
 			min:0
 		},
 		legend: {
 			show: true,
 			position:'ne'
 		},
 		grid: {
 			color: "#AFAFAF",
 			hoverable: true,
 			borderWidth: 0,
 			backgroundColor: '#FFF'
 		},
 		tooltip: true,
 		tooltipOpts: {
 			content: "X: %x, Y: %y",
 			defaultTheme: false
 		}
 	};
 	$.plot($(ini), dataset, options);
 };
});

 </script>
 <script type="text/javascript">
 $('#input_start,#input_end').change(function(){
 	start = $('#input_start').val();
 	end = $('#input_end').val();
 	_start = ((new Date(start)).getTime())/1000;
 	_end = ((new Date(end)).getTime())/1000;

 	$url = $("#input_see").data('url');
 	console.log(_start);
 	$("#input_see").attr('href',$url+'/'+_start+'/sampai/'+_end);
 });
 </script>
 <?=partial('js')?>