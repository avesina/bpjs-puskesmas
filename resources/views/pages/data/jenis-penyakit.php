 <div class="page-title">
 	<div class="container">
 		<h3>Tabel Berdasarkan Umur</h3>
 	</div>
 </div>
 <div id="main-wrapper" class="container">
 	<div class='row'>
 		<div class="col-md-12 col-sm-12 col-xs-12">
 			<div class="panel panel-white">
 				<div class="panel-heading clearfix">
 					<div class="form-inline col-md-11">
 						<div class="input-group input-daterange form-group">
 							<input id="input_start" type="text" class="form-control" value="<?=date("Y-m-d",$start)?>">
 							<span class="input-group-addon"> sampai </span>
 							<input id="input_end" type="text" class="form-control" value="<?=date("Y-m-d",$end)?>">

 						</div>
 						<a id="input_see" data-url="<?=url('data/rangeumur')?>" href="" class="btn btn-info"> lihat </a>
 					</div>

 				</div>
 				<div class="panel-body">
 					<br>
 					<table class="datatable-responsive table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
 						<thead>
 							<tr>
 								<th>Nama Diagnosis</th>
 								<th>0-7 hari</th>
 								<th>8-30 hari</th>
 								<th>1-11 bulan</th>
 								<th>1-4 tahun</th>
 								<th>5-14 tahun</th>
 								<th>15-44 tahun</th>
 								<th>45-64 tahun</th>
 								<th>65 ++ tahun</th>
 							</tr>
 						</thead>
 						<tbody>
 							<?php foreach ($data as $key => $row) { //dd($row->toArray());
 										?>
 										<tr>
 											<td><?=$row->diagnosis?></td>
 											<td><?=$row->{'0-7hari'}?></td>
 											<td><?=$row->{'8-30hari'}?></td>
 											<td><?=$row->{'1-11bulan'}?></td>
 											<td><?=$row->{'1-4'}?></td>
 											<td><?=$row->{'5-14'}?></td>
 											<td><?=$row->{'15-44'}?></td>
 											<td><?=$row->{'45-64'}?></td>
 											<td><?=$row->{'64more'}?></td>
 										</tr>
 										<?php
 									}
 									?>
 								</tbody>
 							</table>

 						</div>
 					</div>
 				</div>
 			</div>
 		</div>
 		<?=partial('main_js')?>
 		<script src="<?=url('')?>/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
 		<script src="<?=url('')?>/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
 		<script type="text/javascript">
 		$('.input-daterange input').each(function() {
 			$(this).datepicker({
 				orientation: "top auto",
 				format: 'yyyy-mm-dd',
 				autoclose: true
 			});
 		});
 		</script>
 		<script type="text/javascript">
 		$('#input_start,#input_end').change(function(){
 			start = $('#input_start').val();
 			end = $('#input_end').val();
 			_start = ((new Date(start)).getTime())/1000;
 			_end = ((new Date(end)).getTime())/1000;

 			$url = $("#input_see").data('url');
 			console.log(_start);
 			$("#input_see").attr('href',$url+'/'+_start+'/sampai/'+_end);
 		});
 		</script>
 		<?=partial('js')?>