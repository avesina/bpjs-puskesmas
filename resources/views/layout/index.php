<!DOCTYPE html>
<html>
<head>
    <?=partial('head')?>
</head>
      <body class="page-header-fixed compact-menu page-horizontal-bar">
        <div class="overlay"></div>

        <!-- seacr menu -->
        <form class="search-form" action="#" method="GET">
          <div class="input-group">
            <input type="text" name="search" class="form-control search-input" placeholder="Search...">
            <span class="input-group-btn">
              <button class="btn btn-default close-search waves-effect waves-button waves-classic" type="button"><i class="fa fa-times"></i></button>
            </span>
          </div><!-- Input Group -->
        </form><!-- Search Form -->

        <main class="page-content content-wrap">
          <div class="navbar">
              <?=partial('headermenu')?>
          </div><!-- Navbar -->
          <div class="page-sidebar sidebar horizontal-bar">
            <div class="page-sidebar-inner">
              <?=partial('menu')?>
            </div><!-- Page Sidebar Inner -->
          </div><!-- Page Sidebar -->
          <div class="page-inner">
              <?=pages($page)?>
          <!-- Main Wrapper -->
            <?=partial('footer')?>
        </div><!-- Page Inner -->
      </main><!-- Page Content -->
      <div class="cd-overlay"></div>
      <!-- Javascripts -->
    </body>

    <!-- Mirrored from lambdathemes.in/admin2/index.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 20 May 2016 02:54:06 GMT -->
    </html>