<!DOCTYPE html>
<html>
    
<!-- Mirrored from lambdathemes.in/admin2/login.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 20 May 2016 02:55:37 GMT -->
<head>
  <?=partial('head')?>
</head>
    <body class="page-login">
        <main class="page-content">
            <div class="page-inner">
                <div id="main-wrapper">
                    <div class="row">
                        <div class="col-md-4 center">
                            <div class="login-box">
                                <a href="index.html" class="logo-name text-lg text-center">Sistem Informasi Rawat Jalan</a>
                                <p class="text-center m-t-md">Silahkah Login</p>
                                <form class="m-t-md ajaxsubmit2" method="post" action="<?=url('login')?>">
                                    <div class="form-group">
                                        <input name='username' type="username" class="form-control" placeholder="username" required>
                                    </div>
                                    <div class="form-group">
                                        <input name='password' type="password" class="form-control" placeholder="Password" required>
                                    </div>
                                    <button type="submit" class="btn btn-success btn-block">Login</button>
                                </form>
                                <p class="text-center m-t-xs text-sm">2016 &copy; nuravesinamustari@gmail.com</p>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
            </div><!-- Page Inner -->
        </main><!-- Page Content -->
  

        <!-- Javascripts -->
        <?=partial('main_js')?>
        <?=partial('js')?>
        
    </body>

<!-- Mirrored from lambdathemes.in/admin2/login.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 20 May 2016 02:55:37 GMT -->
</html>