<div class='row'>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Tabel Kecamatan</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="btn btn-default" href="<?=url('kecamatan/create')?>"><i class="fa fa-plus"></i> Kecamatan</a></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				
				<br>
				<br>
				<table class="datatable-responsive table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>id</th>
							<th>Kabupaten</th>
							<th>Nama Kecamatan</th>
							<th width='80px'></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($data as $key => $value): ?>
						<tr>
							<td><?=$value->id?></td>
							<th><?=$value->kabupaten()->first()->nama_kabupaten?></th>
							<td><?=$value->nama_kecamatan?></td>
							<td>
								<a class="btn btn-info" href="<?=url('kecamatan/'.$value->id.'/edit')?>"><i class="fa fa-edit"></i></a>
								<button onclick="deleteData(this)" data-url="<?=url('kecamatan/'.$value->id)?>" data-direct="<?=url('/kabupaten')?>" class="btn btn-danger" href="#/pencil-square-o"><i class="fa fa-trash"></i></button> 
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>

			</div>
		</div>
	</div>
</div>