 <?php
 if(isset($kecamatan)){
 	$id = $kecamatan->id;
 	$id_kabupaten = $kecamatan->id_kabupaten;
 	$nama_kecamatan = $kabupaten->nama_kecamatan;
 	$method = "PUT";
 	$action = url('kecamatan/'.$id);
 }else{
 	$method = "POST";
 	$action = url('kecamatan');
 	$id = '';
 	$id_kabupaten = '';
 	$nama_kecamatan = '';
 }
 ?>
 <div class="row">
 	<div class="col-md-12 col-sm-12 col-xs-12">
 		<div class="x_panel">
 			<div class="x_title">
 				<h2>Tambah Kecamatan <small></small></h2>
 				<ul class="nav navbar-right panel_toolbox">
 					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
 				</ul>
 				<div class="clearfix"></div>
 			</div>
 			<div class="x_content">
 				<br />
 				<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left ajaxsubmit" method="<?=$method?>" direct ='<?=url('kecamatan')?>' action="<?=$action?>">
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Kecamatan
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<input name="nama_kecamatan" value="<?=$nama_kecamatan?>" type="text" class="form-control col-md-7 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Terletak di Kabupaten
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<select name="id_kabupaten" id="select_kel" class="form-control">
 								<option></option>
 								<?php foreach (kabupaten() as $key => $value) {
 									$select = "";
 									if($value->id == $id_kabupaten){
 										$select="selected";
 									}
 									echo "<option ".$select." value='".$value->id."' >".$value->nama_kabupaten."</option>";
 								} ?>
 							</select>
 						</div>
 					</div>
 					<div class="ln_solid"></div>
 					<div class="form-group">
 						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
 							<button type="reset" class="btn btn-primary">Cancel</button>
 							<button type="submit" class="btn btn-success">Submit</button>
 						</div>
 					</div>
 				</form>
 			</div>
 		</div>
 	</div>
 </div>