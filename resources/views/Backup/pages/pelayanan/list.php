<div class='row'>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Hasil Pelayanan</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a href="#"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a href="#"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<p class="text-muted font-13 m-b-30">
					Responsive is an extension for DataTables that resolves that problem by optimising the table's layout for different screen sizes through the dynamic insertion and removal of columns from the table.
				</p>
				<table class="datatable-responsive table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Tanggal</th>
							<th>Nama Lengkap</th>
							<th>Jenis Pembayaran</th>
							<th>Jenis Pelayanan</th>
							<th>Jenis Kunjungan</th>
							<th>Keluhan</th>
							<th>Diagnosis</th>

						</tr>
					</thead>
					<tbody>
						<?php foreach ($data as $key => $value): ?>
						<tr>
							<td><?=date('Y-m-d',strtotime($value->created_at))?></td>
							<td><?=$value->pasien->nama_lengkap?></td>
							<td><?=$value->jenis_pembayaran?></td>
							<td><?=$value->pelayanan->nama_layanan?></td>
							<td><?=$value->jenis_kunjungan?></td>
							<td><?=$value->keluhan?></td>
							<td><?=$value->diagnosis->nama_diagnosis?></td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>