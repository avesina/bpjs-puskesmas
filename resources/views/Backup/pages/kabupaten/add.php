 <?php
 if(isset($kabupaten)){
 	$id = $kabupaten->id;
 	$nama_kabupaten = $kabupaten->nama_kabupaten;
 	$method = "PUT";
 	$action = url('kabupaten/'.$id);
 }else{
 	$method = "POST";
 	$action = url('kabupaten');
 	$id = '';
 	$nama_kabupaten = '';
 }
 ?>
 <div class="row">
 	<div class="col-md-12 col-sm-12 col-xs-12">
 		<div class="x_panel">
 			<div class="x_title">
 				<h2>Tambah Kabupaten <small></small></h2>
 				<ul class="nav navbar-right panel_toolbox">
 					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
 				</ul>
 				<div class="clearfix"></div>
 			</div>
 			<div class="x_content">
 				<br />
 				<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left ajaxsubmit" method="<?=$method?>" direct ='<?=url('kabupaten')?>' action="<?=$action?>">
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Kabupaten
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<input name="nama_kabupaten" value="<?=$nama_kabupaten?>" type="text" class="form-control col-md-7 col-xs-12">
 						</div>
 					</div>
 					<div class="ln_solid"></div>
 					<div class="form-group">
 						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
 							<button type="reset" class="btn btn-primary">Cancel</button>
 							<button type="submit" class="btn btn-success">Submit</button>
 						</div>
 					</div>
 				</form>
 			</div>
 		</div>
 	</div>
 </div>