 <style type="text/css">
 input[type='number'] {
 	-moz-appearance:textfield;
 }

 input::-webkit-outer-spin-button,
 input::-webkit-inner-spin-button {
 	-webkit-appearance: none;
 }
 span.right {
 	float: right;
 	font-size: 13px !important;
 	width: 80px;
 }
 </style>
 <?php

 if(isset($pemeriksaan)){
 	$pasien = $pemeriksaan->pasien;
 	$id = $pemeriksaan->id;
 	$tgl_pendaftaran = date('Y-m-d');
 	$jenis_kunjungan = $pemeriksaan->jenis_kunjungan;
 	$perawatan = $pemeriksaan->perawatan;
 	$jenis_pelayanan = $pemeriksaan->jenis_pelayanan;
 	$keluhan = $pemeriksaan->keluhan;
 	$tinggi_badan = $pemeriksaan->tinggi_badan;
 	$berat_badan = $pemeriksaan->berat_badan;
 	$sistole = $pemeriksaan->sistole;
 	$diastole = $pemeriksaan->diastole;
 	$respiratory_rate = $pemeriksaan->respiratory_rate;
 	$heart_rate = $pemeriksaan->heart_rate;
 	$id_diagnosis = $pemeriksaan->id_diagnosis;
 	$nama_diagnosis = '';
 	if($pemeriksaan->diagnosis){
 		$nama_diagnosis = $pemeriksaan->diagnosis->nama_diagnosis;
 	}
 	$keterangan = $pemeriksaan->keterangan;
 	$jenis_pembayaran = $pemeriksaan->jenis_pembayaran;
 	$terapi = $pemeriksaan->terapi;
 }

 if(isset($pasien)){
 	$kdfaskes = $pasien->kdfaskes;
 	$namafaskes = $pasien->namafaskes;
 	$noktp = $pasien->noktp;
 	$nobpjs = $pasien->nobpjs;
 	$noka = $pasien->noka;
 	$nokk = $pasien->nokk;
 	$nosin = $pasien->nosin;
 	$nama_lengkap = $pasien->nama_lengkap;
 	$jenis_kelamin = $pasien->jenis_kelamin;
 	$tgl_lahir = $pasien->tgl_lahir;
 	$pisa = $pasien->pisa;
 	$alamat = $pasien->alamat;
 	$kecamatan = $pasien->kecamatan;
 	$kelurahan = $pasien->kelurahan;
 }

 ?>
 <div class="row">
 	<div class="col-md-5 col-sm-12 col-xs-12">
 		<div class="x_panel">
 			<div class="x_title">
 				<h2>Pasien <small></small></h2>
 				<ul class="nav navbar-right panel_toolbox">
 					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
 				</ul>
 				<div class="clearfix"></div>
 			</div>
 			<div class="x_content">
 				<br />
 				<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left ajaxsubmit" method="POST" direct = '' action="<?=url('pasien')?>">
 					<div class="form-group">
 						<label class="control-label col-md-4 col-sm-3 col-xs-12" for="first-name">No. Rekam Medis</label>
 						<div class="col-md-8 col-sm-8 col-xs-12">
 							<input id="no_rekam_medis" readonly="readonly" value="<?=$nobpjs?>" type="text" class="form-control col-md-12 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-4 col-sm-3 col-xs-12" for="first-name">No. Rekam Medis Lama</label>
 						<div class="col-md-8 col-sm-8 col-xs-12">
 							<input id="no_rekam_medis_lama" readonly="readonly" value="<?=$nobpjs?>" type="text" class="form-control col-md-12 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-4 col-sm-3 col-xs-12" for="first-name">No. kartu bpjs</label>
 						<div class="col-md-8 col-sm-8 col-xs-12">
 							<input id="nobpjs" readonly="readonly" value="<?=$nobpjs?>" type="text" class="form-control col-md-12 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-4 col-sm-3 col-xs-12" for="first-name">nama lengkap</label>
 						<div class="col-md-8 col-sm-8 col-xs-12">
 							<input id="nama_lengkap" readonly="readonly" value="<?=$nama_lengkap?>" type="text" class="form-control col-md-12 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-4 col-sm-3 col-xs-12" for="first-name">status peserta</label>
 						<div class="col-md-8 col-sm-8 col-xs-12">
 							<input id="status_peserta" readonly="readonly" value="<?=''?>" type="text" class="form-control col-md-12 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-4 col-sm-3 col-xs-12" for="first-name">jenis peserta</label>
 						<div class="col-md-8 col-sm-8 col-xs-12">
 							<input id="jenis_peserta" readonly="readonly" value="<?=''?>" type="text" class="form-control col-md-12 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-4 col-sm-3 col-xs-12" for="first-name">tanggal lahir</label>
 						<div class="col-md-8 col-sm-8 col-xs-12">
 							<input id="tgl_lahir" readonly="readonly" value="<?=$tgl_lahir?>" type="text" class="form-control col-md-12 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-4 col-sm-3 col-xs-12" for="first-name">jenis kelamin</label>
 						<div class="col-md-8 col-sm-8 col-xs-12">
 							<input id="jenis_kelamin" readonly="readonly" value="<?=$jenis_kelamin?>" type="text" class="form-control col-md-12 col-xs-12">
 						</div>
 					</div>
 				</form>
 			</div>
 		</div>

 		
 	</div>
 	<div class="col-md-7 col-sm-12 col-xs-12">
 		<div class="x_panel">
 			<div class="x_title">
 				<h2>Pelayanan <small></small></h2>
 				<ul class="nav navbar-right panel_toolbox">
 					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
 				</ul>
 				<div class="clearfix"></div>
 			</div>
 			<div class="x_content">
 				<br />
 				<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left ajaxsubmit" method="PUT" action="<?=url('pemeriksaan/'.$id)?>">
 					<input id="id_pasien" value="" type="hidden" name="id_pasien">
 					<div class="form-group">
 						<label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">tanggal pendaftaran 
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<input name="tgl_pendaftaran" readonly="readonly" value="<?=$tgl_pendaftaran?>" type="text" class="form-control col-md-7 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">jenis kunjungan 
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<input value="<?=$jenis_kunjungan?>" id='jeniskunjungan' name="jenis_kunjungan" readonly="readonly" type="text" class="form-control col-md-7 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">perawatan
 						</label>
 						<div class="col-md-8 col-sm-8 col-xs-12">
 							rawat jalan : <input type="radio" class="flat" name="perawatan" id="genderM" value="rawat jalan" checked="" required /> &nbsp&nbsp&nbsp rawat inap :
 							<input name="perawatan" type="radio" class="flat" name="perawatan" id="genderF" value="rawat inap" />
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">jenis pembayaran
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<select name="jenis_pembayaran" class="form-control">
 								<option <?=($jenis_pembayaran=='umum'?'selected':'')?> value='umum'>Umum</option>
 								<option <?=($jenis_pembayaran=='bpjs'?'selected':'')?> value='bpjs'>BPJS</option>
 								<option <?=($jenis_pembayaran=='lainnya'?'selected':'')?> value='lainnya'>Lainnya</option>
 							</select>
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">jenis pelayanan
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<?php $jl = jenis_pelayanan();?>
 							<select name="jenis_pelayanan" class="form-control">
 								<?php 
 								foreach ($jl as $key => $value) {
 									$select = ($jenis_pelayanan==$value->nama_layanan?'selected':'');
 									echo "<option ".$select."  value='".$value->id."'>".$value->nama_layanan."</option>";
 								}
 								?>
 							</select>
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Jenis Kasus
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<select name="jenis_kasus" class="form-control">
 								<option value=''>-- pilih --</option>
 								<option value='baru'>Baru</option>
 								<option value='lama'>Lama</option>
 								<option value='kunjungan kasus'>Kunjungan Kasus</option>
 							</select>
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">keluhan 
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<textarea name="keluhan" rows="7" class="form-control" ><?=$keluhan?></textarea>
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Terapi 
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<textarea name="terapi" rows="7" class="form-control" ><?=$terapi?></textarea>
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Diagnosis 
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							 <div class="input-group">
 							<input id="id_diagnosis" name="id_diagnosis" value="<?=$id_diagnosis?>" type="hidden" class="form-control col-md-7 col-xs-12">
 							<input readonly="readonly" id="nama_diagnosis" name="nama_diagnosis" value="<?=$nama_diagnosis?>" type="text" class="form-control col-md-7 col-xs-12">
 							<span class="input-group-btn">
 								<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal_diagnosis"><i class="fa fa-hand-o-up"></i></a>
 							</span>
 						</div>
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Keterangan 
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<textarea name="keterangan" rows="7" class="form-control" ><?=$keterangan?></textarea>
 						</div>
 					</div>
 					<fieldset>
 						<legend>Pemeriksaan fisik:</legend>
 						<div class="form-group">
 							<label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">tinggi badan 
 							</label>
 							<div class="col-md-6 col-sm-6 col-xs-12">
 								<input name="tinggi_badan" maxlength="10" type="number" class="form-control">
 								<span class="form-control-feedback right" aria-hidden="true">cm</span>
 							</div>
 						</div>
 						<div class="form-group">
 							<label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">berat badan 
 							</label>
 							<div class="col-md-6 col-sm-6 col-xs-12">
 								<input name="berat_badan" value="<?=$berat_badan?>" type="number" class="form-control col-md-7 col-xs-12">
 								<span class="form-control-feedback right" aria-hidden="true">kg</span>
 							</div>
 						</div>
 					</fieldset>
 					<br>
 					<fieldset>
 						<legend>Tekanan darah:</legend>
 						<div class="form-group">
 							<label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">sistole 
 							</label>
 							<div class="col-md-6 col-sm-6 col-xs-12">
 								<input name="sistole" value="<?=$sistole?>" type="number" class="form-control col-md-7 col-xs-12">
 								<span class="form-control-feedback right" aria-hidden="true">mmHg</span>
 							</div>
 						</div>
 						<div class="form-group">
 							<label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">diastole 
 							</label>
 							<div class="col-md-6 col-sm-6 col-xs-12">
 								<input name="diastole" value="<?=$diastole?>" type="number" class="form-control">
 								<span class="form-control-feedback span5 right" aria-hidden="true">mmHg</span>
 							</div>
 						</div>
 						<div class="form-group">
 							<label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">respiratory rate 
 							</label>
 							<div class="col-md-6 col-sm-6 col-xs-12">
 								<input name="respiratory_rate" maxlength="10" value="<?=$respiratory_rate?>" type="number" class="form-control col-md-7 col-xs-12">
 								<span class="form-control-feedback right" aria-hidden="true">per minute</span>
 							</div>
 						</div>
 						<div class="form-group">
 							<label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">heart rate 
 							</label>
 							<div class="col-md-6 col-sm-6 col-xs-12">
 								<input name="heart_rate" value="<?=$heart_rate?>" type="number" class="form-control col-md-7 col-xs-12">
 								<span class="form-control-feedback right" aria-hidden="true">bpm</span>
 							</div>
 						</div>
 					</fieldset>

 					
 					<div class="ln_solid"></div>
 					<div class="form-group">
 						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
 							<button type="reset" class="btn btn-primary">Cancel</button>
 							<button type="submit" class="btn btn-success">Submit</button>
 						</div>
 					</div>
 				</form>
 			</div>
 		</div>
 	</div>
 </div>
 <!-- Modal -->
 <style type="text/css">
 .dataTables_filter{
 	width: 100%;
 }
 #modal_diagnosis table tr{
 	cursor: pointer;
 }
 </style>
 <div class="modal fade" id="modal_diagnosis" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 	<div class="modal-dialog" role="document">
 		<div class="modal-content">
 			<div class="modal-header">
 				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 				<h4 class="modal-title" id="myModalLabel">Diagnosis</h4>
 			</div>
 			<div class="modal-body">
 				<div class="x_panel">
 					<div class="x_content">
 						<table class="datatable-diagnosis hover table table-bordered dt-responsive nowrap"  btn-invisible='true' cellspacing="0" width="100%">
 							<thead>
 								<tr>
 									<th width='10'>id</th>
 									<th width='20'>Kode </th>
 									<th>Nama Diagnosis</th>
 								</tr>
 							</thead>
 							<tbody>

 							</tbody>
 						</table>
 					</div>
 				</div>
 			</div>
 		</div>
 	</div>
 </div>