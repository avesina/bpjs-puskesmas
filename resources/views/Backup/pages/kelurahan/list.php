<div class='row'>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Tabel Kelurahan</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="btn btn-default" href="<?=url('kelurahan/create')?>"><i class="fa fa-plus"></i> Kelurahan</a></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				
				<br>
				<br>
				<table class="datatable-responsive table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>id</th>
							<th>Kecamatan</th>
							<th>Nama Kelurahan</th>
							<th width='80px'></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($data as $key => $value): ?>
						<tr>
							<td><?=$value->id?></td>
							<th><?=$value->kecamatan()->first()->nama_kecamatan?></th>
							<td><?=$value->nama_kelurahan?></td>
							<td>
								<a class="btn btn-info" href="<?=url('kelurahan/'.$value->id.'/edit')?>"><i class="fa fa-edit"></i></a>
								<button onclick="deleteData(this)" data-url="<?=url('kelurahan/'.$value->id)?>" data-direct="<?=url('/kabupaten')?>" class="btn btn-danger" href="#/pencil-square-o"><i class="fa fa-trash"></i></button> 
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>

			</div>
		</div>
	</div>
</div>