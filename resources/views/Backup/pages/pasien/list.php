<div class='row'>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Tabel Pasien</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a href="#"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a href="#"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<p class="text-muted font-13 m-b-30">
					Responsive is an extension for DataTables that resolves that problem by optimising the table's layout for different screen sizes through the dynamic insertion and removal of columns from the table.
				</p>
				<table class="datatable-responsive table table-striped table-bordered dt-responsive nowrap" data-visible ="2,3,4,5,6,10,11" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>no rekam medis</th>
							<th>no bpjs</th>
							<th>kdfaskes</th>
							<th>nama faskes</th>
							<th>noka</th>
							<th>no KK</th>
							<th>no SIN</th>
							<th>Nama Lengkap</th>
							<th>Tanggal lahir</th>
							<th>Umur</th>
							<th>PISA</th>
							<th>Alamat</th>
							<th>Kab</th>
							<th>Kec</th>
							<th>Kel</th>
							<th>action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($data as $key => $value): ?>
						<tr>
							<td><?=$value->no_rekam_medis?></td>
							<td><?=$value->nobpjs?></td>
							<td><?=$value->kdfaskes?></td>
							<td><?=$value->namafaskes?></td>
							<td><?=$value->noka?></td>
							<td><?=$value->nokk?></td>
							<td><?=$value->nosin?></td>
							<td><?=$value->nama_lengkap?></td>
							<td><?=$value->tgl_lahir?></td>
							<td><?=calculateAge($value->tgl_lahir)?></td>
							<td><?=$value->pisa?></td>
							<td><?=$value->alamat?></td>
							<td><?=(($value->kabupaten)?$value->kabupaten->nama_kabupaten:'')?></td>
							<td><?=(($value->kecamatan)?$value->kecamatan->nama_kecamatan:'')?></td>
							<td><?=(($value->kelurahan)?$value->kelurahan->nama_kelurahan:'')?></td>
							<td>
								<a class="btn btn-info" href="<?=url('pasien/'.$value->id.'/edit')?>"><i class="fa fa-edit"></i></a>
								<button onclick="deleteData(this)" data-url="<?=url('pasien/'.$value->id)?>" data-direct="<?=url('/kabupaten')?>" class="btn btn-danger" href="#/pencil-square-o"><i class="fa fa-trash"></i></button> 
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>

			</div>
		</div>
	</div>
</div>