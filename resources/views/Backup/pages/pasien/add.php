 <?php
 if(isset($pasien)){
 	$method = 'PUT';
 	$action = url('pasien/'.$pasien->id);
 	$no_rekam_medis = $pasien->no_rekam_medis;
 	$no_rekam_medis_lama = $pasien->no_rekam_medis_lama;
 	$noktp = $pasien->noktp;
 	$nobpjs = $pasien->nobpjs;
 	$kdfaskes = $pasien->kdfaskes;
 	$namafaskes = $pasien->namafaskes;
 	$noka = $pasien->noka;
 	$nokk = $pasien->nokk;
 	$nosin = $pasien->nosin;
 	$nama_lengkap = $pasien->nama_lengkap;
 	$jenis_kelamin = $pasien->jenis_kelamin;
 	$tgl_lahir = $pasien->tgl_lahir;
 	$pisa = $pasien->pisa;
 	$alamat = $pasien->alamat;
 	$id_kabupaten = $pasien->id_kabupaten;
 	$id_kecamatan = $pasien->id_kecamatan;
 	$id_kelurahan = $pasien->id_kelurahan;
 }else{
 	$method = 'POST';
 	$action = url('pasien');
 	$no_rekam_medis = '';
 	$no_rekam_medis_lama = '';
 	$noktp = '';
 	$nobpjs = '';
 	$kdfaskes = '';
 	$namafaskes = '';
 	$noka = '';
 	$nokk = '';
 	$nosin = '';
 	$nama_lengkap = '';
 	$jenis_kelamin = '';
 	$tgl_lahir = '';
 	$pisa = '';
 	$alamat = '';
 	$id_kabupaten = '';
 	$id_kecamatan = '';
 	$id_kelurahan = '';

 	// $kdfaskes = rand (0 ,99999);
 	// $namafaskes = generateRandomString();
 	// $noktp = rand (0 ,99999);
 	// $nobpjs = rand (0 ,99999);
 	// $noka = rand (0 ,99999);
 	// $nokk = rand (0 ,99999);
 	// $nosin = rand (0 ,99999);
 	// $nama_lengkap = generateRandomString();
 	// $tgl_lahir = '1990-04-10';
 	// $pisa = rand (0 ,99999);
 	// $alamat = generateRandomString();
 	// $kecamatan = '1';
 	// $kelurahan = '1';
 }
 ?>
 <div class="row">
 	<div class="col-md-12 col-sm-12 col-xs-12">
 		<div class="x_panel">
 			<div class="x_title">
 				<h2>Tambah Pasien <small></small></h2>
 				<ul class="nav navbar-right panel_toolbox">
 					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
 				</ul>
 				<div class="clearfix"></div>
 			</div>
 			<div class="x_content">
 				<br />
 				<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left ajaxsubmit" method="POST" direct = '' action="<?=url('pasien')?>">
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">no rekam medis
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<input name="no_rekam_medis" value="<?=$no_rekam_medis?>" type="text" class="form-control col-md-7 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">no rekam medis lama
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<input name="no_rekam_medis_lama" value="<?=$no_rekam_medis_lama?>" type="text" class="form-control col-md-7 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">noktp
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<input name="noktp" value="<?=$noktp?>" type="text" class="form-control col-md-7 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">no bpjs
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<input name="nobpjs" value="<?=$nobpjs?>" type="text" class="form-control col-md-7 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">kdfaskes 
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<input name="kdfaskes" value="<?=$kdfaskes?>" type="text" class="form-control col-md-7 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">namafaskes 
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<input name="namafaskes" value="<?=$namafaskes?>" type="text" class="form-control col-md-7 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">noka 
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<input name="noka" value="<?=$noka?>" type="text" class="form-control col-md-7 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">nokk 
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<input name="nokk" value="<?=$nokk?>" type="text" class="form-control col-md-7 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">nosin 
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<input name="nosin" value="<?=$nosin?>" type="text" class="form-control col-md-7 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">nama lengkap 
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<input name="nama_lengkap" value="<?=$nama_lengkap?>" type="text" class="form-control col-md-7 col-xs-12">
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">jenis kelamin 
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							male : <input type="radio" class="flat" name="jenis_kelamin" id="genderM" value="male" checked="" required /> &nbsp&nbsp&nbsp female :
 							<input name="jenis_kelamin" type="radio" class="flat" id="genderF" value="female" />
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">tanggal lahir
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12 form-group">
 							<input name="tgl_lahir" placeholder='yyyy-mm-dd' value="<?=$tgl_lahir?>" type="text" class="form-control has-feedback-left single_cal" id="single_cal1" aria-describedby="inputSuccess2Status">
 							<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
 						</div>
 					</div>
 						<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">pisa
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<select name="pisa" class="form-control" tabindex="-1">
 								<option value="peserta" >Peserta</option>
 								<option value="istri/suami">Istri / Suami</option>
 								<option value="anak" >Anak</option>
 							</select>
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">kabupaten
 						</label>
 						<?php $data_kab = kabupaten(); ?>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<select name="id_kabupaten" id="select_kab" class="select2_single form-control" tabindex="-1">
 								<option></option>
 								<?php foreach ($data_kab as $key => $value) {
 									$selected = '';
 									if($value->id == $id_kabupaten){
 										$selected = "selected";
 									}
 									echo "<option ".$selected." value='".$value->id."'>".$value->nama_kabupaten."</option>";
 								} ?>
 							</select>
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">kecamatan 
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<select name="id_kecamatan" id="select_kec" class="form-control" tabindex="-1">
 								<option></option>
 							</select>
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">kelurahan
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<select name="id_kelurahan" id="select_kel" class="form-control">
 								<option></option>
 							</select>
 						</div>
 					</div>
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">alamat 
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-12">
 							<textarea name="alamat" class="form-control" ><?=$alamat?></textarea>
 						</div>
 					</div>
 					<div class="ln_solid"></div>
 					<div class="form-group">
 						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
 							<button type="reset" class="btn btn-primary">Cancel</button>
 							<button type="submit" class="btn btn-success">Submit</button>
 						</div>
 					</div>
 				</form>
 			</div>
 		</div>
 	</div>
 </div>