 <div class="row">
 	<div class="col-md-6 col-sm-6 col-xs-6">
 		<div class="x_panel">
 			<div class="x_title">
 				<h2>Import Data Pasien <small></small></h2>
 				<ul class="nav navbar-right panel_toolbox">
 					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
 				</ul>
 				<div class="clearfix"></div>
 			</div>
 			<div class="x_content">
 				<br />
 				<form enctype="multipart/form-data" method="POST" action="<?=url('import/pasien')?>" class="form-horizontal form-label-left">
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">file 
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-6">
 							<input accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" class="form-control col-md-7 col-xs-12" name="excel" type="file"></input>
 						</div>
 					</div>
 					<div class="ln_solid"></div>
 					<div class="form-group">
 						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
 							<button type="reset" class="btn btn-primary">Cancel</button>
 							<button type="submit" class="btn btn-success">Submit</button>
 						</div>
 					</div>
 				</form>
 			</div>
 		</div>
 	</div>
 	<div class="col-md-6 col-sm-6 col-xs-6">
 		<div class="x_panel">
 			<div class="x_title">
 				<h2>Import Data Diagnosis <small></small></h2>
 				<ul class="nav navbar-right panel_toolbox">
 					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
 				</ul>
 				<div class="clearfix"></div>
 			</div>
 			<div class="x_content">
 				<br />
 				<form enctype="multipart/form-data" method="POST" action="<?=url('import/diagnosis')?>" class="form-horizontal form-label-left">
 					<div class="form-group">
 						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">file 
 						</label>
 						<div class="col-md-6 col-sm-6 col-xs-6">
 							<input accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" class="form-control col-md-7 col-xs-12" name="excel" type="file"></input>
 						</div>
 					</div>
 					<div class="ln_solid"></div>
 					<div class="form-group">
 						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
 							<button type="reset" class="btn btn-primary">Cancel</button>
 							<button type="submit" class="btn btn-success">Submit</button>
 						</div>
 					</div>
 				</form>
 			</div>
 		</div>
 	</div>
 </div>