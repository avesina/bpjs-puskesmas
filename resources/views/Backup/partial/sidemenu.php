   <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
      <h3>Menu</h3>
      <ul class="nav side-menu">
        <li><a href="<?=url('/')?>"><i class="fa fa-home"></i> Home </a></li>
        <?php $key = Session::get('nuravesinamustari'); $user = Session::get($key); ?>
        <li><a><i class="fa fa-desktop"></i> Pelayanan <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu" style="display: none">
            <li><a href="<?=url('pasien/create')?>">Tambah Pasien</a>
            </li>
            <li>
              <a href="<?=url('pendaftaran')?>">Pendaftaran</a>
            </li>
            <?php if($user->role==='admin' || $user->role==='dokter'): ?>
            <li>
              <a href="<?=url('pendaftaran/list')?>">List Pendaftar</a>
            </li>
            <li class="hidden">
              <a  href="<?=url('pemeriksaan')?>">Ruang Periksa</a>
            </li>
          <?php endif; ?>
        </ul>
      </li>
      <?php if($user->role==='admin' || $user->role==='dokter'): ?>
      <li><a><i class="fa fa-bar-chart-o"></i> Data Presentation <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu" style="display: none">
          <li><a href="<?=url('pasien')?>">Tabel Pasien</a>
          </li>
          <li><a href="<?=url('pelayanan')?>">Tabel Kunjungan</a>
          </li>
          <li>
            <a>Hasil Kunjungan</a>
            <ul class="nav child_menu" style="display: none">
                <li>Jenis Umur</li>
                <li>Jenis Pembayaran, Pelayanan, Jenis Kelamin</li>
            </ul>
          </li>
        </ul>
      </li>
    <?php endif; ?>
    <?php if($user->role==='admin'): ?>
    <li>
      <a><i class="fa fa-bar-chart-o"></i> Master Data <span class="fa fa-chevron-down"></span></a>
      <ul class="nav child_menu" style="display: none">
        <li><a href="<?=url('import')?>">Import Data</a>
        </li>
        <li><a href="<?=url('kabupaten')?>">Data Kabupaten</a>
        </li>
        <li><a href="<?=url('kecamatan')?>">Data Kecamatan</a>
        </li>
        <li><a href="<?=url('kelurahan')?>">Data Kelurahan</a>
        </li>
        <li><a href="<?=url('diagnosis')?>">Diagnosis</a>
        </li>
      </ul>
    </li>
  <?php endif; ?>
</ul>
</div>
</div>