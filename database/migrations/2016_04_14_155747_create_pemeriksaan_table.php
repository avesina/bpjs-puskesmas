<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemeriksaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       if (Schema::hasTable('pemeriksaan')) {
        // script for update
       }else{
           Schema::create('pemeriksaan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pasien');
            $table->integer('id_dokter')->nullable();
            $table->dateTime('tgl_pendaftaran');
            $table->enum('jenis_pembayaran',['umum','bpjs','lainnya']);
            $table->enum('jenis_kunjungan',['baru','lama'])->nullable();
            $table->enum('perawatan',['rawat jalan','rawat inap'])->nullable();
            $table->enum('jenis_kasus',['baru','lama','kunjungan kasus'])->nullable();
            $table->integer('id_jenis_pelayanan');
            $table->float('tinggi_badan')->nullable();
            $table->float('berat_badan')->nullable();
            $table->float('sistole')->nullable();
            $table->float('diastole')->nullable();
            $table->enum('kesadaran',['compos mentis'])->nullable();
            $table->float('respiratory_rate')->nullable();
            $table->float('heart_rate')->nullable();
            $table->longText('keluhan')->nullable();
            $table->longText('terapi')->nullable();
            $table->integer('id_diagnosis')->nullable();
            $table->longText('keterangan')->nullable();
            $table->enum('status',['daftar','rawat']);
            $table->timestamps();
        });
       }
   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
