<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiagnosisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('diagnosis')) {
        // script for update
        }else{
           Schema::create('diagnosis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode');
            $table->string('nama_diagnosis');
            $table->string('keterangan');
            $table->timestamps();
        });
       }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
