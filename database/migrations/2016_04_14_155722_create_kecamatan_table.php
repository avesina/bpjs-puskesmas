<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKecamatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('kecamatan')) {
        // script for update
        }else{
           Schema::create('kecamatan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_kabupaten');
            $table->string('nama_kecamatan');
            $table->timestamps();
        });
       }
   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
