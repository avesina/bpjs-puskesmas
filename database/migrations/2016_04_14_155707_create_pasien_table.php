<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasienTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('pasien')) {
        // script for update
        }else{
         Schema::create('pasien', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_rekam_medis')->nullable();
            $table->string('no_rekam_medis_lama')->nullable();
            $table->string('noktp')->nullable();
            $table->string('nobpjs')->nullable();
            $table->mediumInteger('kdfaskes')->nullable();
            $table->string('namafaskes')->nullable();
            $table->string('noka')->nullable();;
            $table->mediumInteger('nokk')->nullable();
            $table->integer('nosin')->nullable();
            $table->string('nama_lengkap');
            $table->enum('jenis_kelamin',['male','female']);
            $table->date('tgl_lahir')->nullable();
            $table->enum('pisa',['peserta','istri/suami','anak'])->nullable();
            $table->string('alamat')->nullable();
            $table->integer('id_kelurahan')->nullable();
            $table->integer('id_kecamatan')->nullable();
            $table->integer('id_kabupaten')->nullable();
            $table->timestamps();
        });
     }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pasien');
    }
}
