<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKelurahanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('kelurahan')) {
        // script for update
        }else{
        Schema::create('kelurahan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_kecamatan');
            $table->string('nama_kelurahan');
            $table->timestamps();
        });
    }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
