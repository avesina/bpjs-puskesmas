<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJenisPelayananTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('jenis_pelayanan')) {
        // script for update
        }else{
         Schema::create('jenis_pelayanan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_layanan');
            $table->timestamps();
        });
     }
 }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
